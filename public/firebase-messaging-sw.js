importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

// Initialize Firebase
var config = {
    apiKey: "AIzaSyAJTSi9ZJIpljnSDaCZ3WV_JdrT5hbn2-A",
    authDomain: "vaithme-df9fe.firebaseapp.com",
    messagingSenderId: "1038721988653"
};
firebase.initializeApp(config);

// INIT FIREBASE MESSAGING
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    var notificationTitle = 'Hallo firebase';
    var notificationOptions = {
        body: payload.data.status,
        icon: '/xampp/favicon.ico'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});