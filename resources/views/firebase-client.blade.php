<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Test Firebase</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>


</head>
<body>
<h1>
    <h1 id="fw">Tes firebase</h1>
    <button id="kirim">Kirim Notifikasi</button>
    <button id="kirim-sms">Kirim SMS</button>
</h1>
<script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-messaging.js"></script> -->
<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>
<script>
    // Initialize Firebase
    // WEB API KEY ON FIREBASE PROJECT CONSOLE
    var config = {
        
        apiKey: "AIzaSyAJTSi9ZJIpljnSDaCZ3WV_JdrT5hbn2-A",
        authDomain: "vaithme-df9fe.firebaseapp.com",
        databaseURL: "https://vaithme-df9fe.firebaseio.com",
        projectId: "vaithme-df9fe",
        storageBucket: "vaithme-df9fe.appspot.com",
        messagingSenderId: "1038721988653"
    };
    firebase.initializeApp(config);

    // INIT FIREBASE MESSAGING
    const messaging = firebase.messaging();
    messaging.requestPermission()
        .then(function () {
            console.log('Notification permission granted.');
            return messaging.getToken();
        })
        .then(function (token) {
            console.log(token);
        })
        .catch(function (err) {
            console.log('Unable to get permission to notify.', err);
        });

    // // TERIMA PESAN
    // // KALAU LAGI DIBUKA BROWSER NYA MASUK SINI
    messaging.onMessage(function (payload) {
        document.getElementById("fw").innerHTML = payload.data.titulo;
        console.log('Message received. ', payload);
    });
    // // KALAU LAGI GA DIBUKA BROWSER NYA MASUK SINI
    // messaging.setBackgroundMessageHandler(function (payload) {
    //     console.log('[firebase-messaging-sw.js] Received background message ', payload);
    //     // Customize notification here
    //     var notificationTitle = 'Background Message Title';
    //     var notificationOptions = {
    //         body: 'Background Message body.',
    //         icon: '/firebase-logo.png'
    //     };
    //
    //     return self.registration.showNotification(notificationTitle,
    //         notificationOptions);
    // });

    document.getElementById("kirim").addEventListener('click', function () {
            // DEVICE TOKEN - LIUS PUNYA
            // ANDROID - cuAqApEthv8:APA91bH96e7cbS2UUQA7Fr31oqtaNyI9Ba6MYzCvXG1Y3dtthaJFQY-1l0YRYYQvLkvEPasJDLKvV9OGVSeKVaBr9HDDqpVqMUsMAok-4FV6vCFyEmYhq4rfE82WhfmrNNhUeK_-fxVG0mL2rp5fu-UvhZJ8G4RJWA
            // OPERA - cV2um8MelRE:APA91bEOEx2WVeoQYpr__XTRw0Ifz1gF8IM7BoXy7fXvdsM734WwSv6rWSV39Q19l4bQ76peq4etZG5fWexhx51TLy1HYKCf1GXIMTUKd5t0BhHfbD0msvhtoxh8QRq8aZfpVGj25erO
            // CHROME - f9CD5W5HCfI:APA91bGuOD0BdiTOHHcEMjplVtOTYuA5hEb7QXArnqs4SwnQ31bBbLCI55pKrUoNsirpQ96-_wejIKIyQ5oXvnnmiWRkydhxOwm7XGTTuBI2dk1GmoSh8Cvx18ZrVyjfY40WQ88ATpax
            var json = {
                "registration_ids": [
                    "cuAqApEthv8:APA91bH96e7cbS2UUQA7Fr31oqtaNyI9Ba6MYzCvXG1Y3dtthaJFQY-1l0YRYYQvLkvEPasJDLKvV9OGVSeKVaBr9HDDqpVqMUsMAok-4FV6vCFyEmYhq4rfE82WhfmrNNhUeK_-fxVG0mL2rp5fu-UvhZJ8G4RJWA",
                    "f9CD5W5HCfI:APA91bGuOD0BdiTOHHcEMjplVtOTYuA5hEb7QXArnqs4SwnQ31bBbLCI55pKrUoNsirpQ96-_wejIKIyQ5oXvnnmiWRkydhxOwm7XGTTuBI2dk1GmoSh8Cvx18ZrVyjfY40WQ88ATpax"
                ],
                // "to": "cuAqApEthv8:APA91bH96e7cbS2UUQA7Fr31oqtaNyI9Ba6MYzCvXG1Y3dtthaJFQY-1l0YRYYQvLkvEPasJDLKvV9OGVSeKVaBr9HDDqpVqMUsMAok-4FV6vCFyEmYhq4rfE82WhfmrNNhUeK_-fxVG0mL2rp5fu-UvhZJ8G4RJWA",
                "notification": {
                    "title": "Tes firebase",
                    "body": "OI OI"
                },
                "data": {
                    "titulo": "Título da mensagem de dados!",
                    "descricao": "Corpo dos dados..."
                }
            };

            $.ajax({
                url: 'https://fcm.googleapis.com/fcm/send',
                type: "POST",
                processData: false,
                beforeSend: function (xhr) {
                    // LEGACY SERVER KEY ON FIREBASE PROJECT
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    xhr.setRequestHeader('Authorization', 'key=AIzaSyD2Pw1G5qawlr0TcBGBJ4JiWJnO4Rwf8VA');
                },
                data: JSON.stringify(json),
                success: function () {
                    console.log("Mensagem enviada com sucesso!");
                },
                error: function (error) {
                    console.log(error);
                }
            });
        },
        false
    );

    /**
     * Login SMS Genie
     * phone : +6285780301994 ~ nomor arief
     * password : vaithme123
     */
    document.getElementById("kirim-sms").addEventListener('click', function () {
        // TODO : COBA DI CARI CARA GUZZLE NYA DEH, GA BISA NIH AJAX GA MAU JALAN
        var json = {
            "to_number": "+62 858-8747-7069",
            "name": "Message Name",
            // GATEWAY SMS DARI SMS GENIE, KITA BUAT DI PANEL SMS GENIE
            "gateway": "1593",
            "message": "Test kirim pesan"
        };

        $.ajax({
            url: 'https://www.smsgenie.co/api/v1/sms/outgoing-sms/',
            type: "POST",
            processData: false,
            beforeSend: function (xhr) {
                // LEGACY SERVER KEY ON FIREBASE PROJECT
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.setRequestHeader('Authorization', 'Token 7bd4f5b01aa6cc0e13a536cd36c9fa798b5b3fd5');
            },
            data: JSON.stringify(json),
            success: function () {
                console.log("Mensagem enviada com sucesso!");
            },
            error: function (error) {
                console.log(error);
            }
        });

        // // API TOKEN DARI SMS GENIE, AMBIL DI PANEL SMS GENIE
        // $.ajax({
        //     url: '',
        //     headers: {
        //         'Authorization': 'Token 7bd4f5b01aa6cc0e13a536cd36c9fa798b5b3fd5',
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json'
        //     },
        //     processData: false,
        //     type: "POST",
        //     data: JSON.stringify(json),
        //     success: function () {
        //         console.log("berhasil");
        //     },
        //     error: function (error) {
        //         console.log(error);
        //     }
        // });

        // var form = new FormData();
        // form.append("to_number", "+62 858-8747-7069");
        // form.append("message", "isi pesan");
        // form.append("gateway", "1593");
        // form.append("name", "pesan");
        //
        // var settings = {
        //     "async": true,
        //     "crossDomain": true,
        //     "url": "https://www.smsgenie.co/api/v1/sms/outgoing-sms/?page=10&page_size=1&ordering=-created",
        //     "method": "GET",
        //     "headers": {
        //         "Authorization": "Token 7bd4f5b01aa6cc0e13a536cd36c9fa798b5b3fd5"
        //         // "Cache-Control": "no-cache",
        //         // "Postman-Token": "55c3daf7-7b9c-42d8-9c8c-f6f18c08cca2"
        //     },
        //     "processData": false,
        //     "contentType": false,
        //     "mimeType": "multipart/form-data"
        //     // "data": form
        // };
        //
        // $.ajax(settings).done(function (response) {
        //     console.log(response);
        // });
    }, false);
</script>
</body>
</html>