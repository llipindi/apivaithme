<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Informasi extends Model
{
    protected $table = 'tb_informasi';
    public $timestamps = false;
    protected $fillable = [
        'KONTAK_TELP','KONTAK_INSTAGRAM','KONTAK_FACEBOOK','KONTAK_EMAIL','ALAMAT'
    ];
}
