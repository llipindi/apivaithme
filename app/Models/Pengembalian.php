<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    protected $table = 'tb_pengembalian';
    public $timestamps = false;
    protected $fillable = [
        'KD_PENGEMBALIAN','KD_RESERVASI','JUMLAH_TANGGUNGAN','STATUS','TGL_UPDATE'
    ];
}
