<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JnsTransportasi extends Model
{
    protected $table = 'tb_jnstransportasi';
    public $timestamps = false;
    protected $fillable = [
        'NAMA_JENISTRANSPORTASI','DESKRIPSI_JENISTRANSPORTASI'
    ];
    public function transportasi() {
		return $this->hasMany('tb_transportasi', 'KD_JENISTRANSPORTASI');
	}
}
