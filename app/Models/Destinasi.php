<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\ObyekWisata;

class Destinasi extends Model
{
    protected $table = 'tb_destinasi';
    public $timestamps = false;
    protected $fillable = [
        'NAMA_DESTINASI'
    ];
    public function obyekwisatamas() {
		return $this->hasMany('App\\ObyekWisata', 'KD_DESTINASI');
	}
    public function detdestinasi() {
		return $this->hasMany('tb_detdestinasi', 'KD_DESTINASI');
	}
}
