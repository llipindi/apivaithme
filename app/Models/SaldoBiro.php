<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaldoBiro extends Model
{
    protected $table = 'tb_saldobiro';
    public $timestamps = false;
    protected $fillable = [
        'KD_BIROTRAVEL', 'TOTAL','TGL_UPDATE'
    ];
}
