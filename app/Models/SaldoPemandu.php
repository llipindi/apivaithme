<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaldoPemandu extends Model
{
    protected $table = 'tb_saldopemandu';
    public $timestamps = false;
    protected $fillable = [
        'KD_PEMANDUWISATA', 'TOTAL','TGL_UPDATE'
    ];
}
