<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InclusionDet extends Model
{
    protected $table = 'tb_inclusiondet';
    public $timestamps = false;
    protected $fillable = [
        'KD_INCLUSION','KD_PAKETWISATA'
    ];
    public function paketwisata() {
		return $this->belongsTo('App\\PaketWisata', 'KD_PAKETWISATA');
    }
}
