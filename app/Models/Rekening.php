<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    use Notifiable;
    protected $table = 'tb_rekening';
    public $timestamps = false;
    protected $fillable = [
        'KD_AKUN', 'NO_REKENING', 'BANK','NAMA_REKENING'
    ];
    public function akun() {
		return $this->belongsTo('tb_akun', 'KD_AKUN');
    }
}
