<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BiroTravel extends Model
{
    protected $table = 'tb_birotravel';
    public $timestamps = false;
    protected $fillable = [
        'KD_AKUN', 'NAMA_BIRO', 'ALAMAT_BIRO', 'NOTELP_BIRO','NAMAPJ_BIRO','NIKPJ_BIRO','EMAILPJ_BIRO','JABATANPJ_BIRO',
    ];
    public function akun() {
		return $this->belongsTo('tb_akun', 'KD_AKUN');
    }
    public function penginapan() {
		return $this->hasMany('tb_penginapan', 'KD_BIROTRAVEL');
	}
    protected $hidden = [
        'KD_AKUN', 'TGL_UPDATE'
    ];
}
