<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PemanduWisata extends Model
{
    protected $table = 'tb_pemanduwisata';
    public $timestamps = false;
    protected $fillable = [
        'KD_AKUN', 'NAMA_PEMANDUWISATA', 'NIK_PEMANDUWISATA', 'JK_PEMANDUWISATA','TL_PEMANDUWISATA','NO_PEMANDUWISATA','PENDIDIKAN','DOMISILI','PENGALAMAN_PEMANDUWISATA'
    ];
    public function akun() {
		return $this->belongsTo('tb_akun', 'KD_AKUN');
	}
    protected $hidden = [
        'KD_AKUN', 'TGL_UPDATE'
    ];
}
