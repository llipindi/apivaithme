<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaketWisata extends Model
{
    protected $table = 'tb_paketwisata';
    public $timestamps = false;
    protected $fillable = [
        'JUDUL_PAKET','DESKRIPSI_PAKET','TOTAL','KD_BIROTRAVEL','KD_DESTINASI','KD_PENGINAPAN','KD_TIPEWISATA','KD_RESERVASI','STATUS_PRAMUWISATA','STATUS_PAKET'
    ];
    public function pembayaran() {
		return $this->hasMany('tb_tagihan', 'KD_PAKETWISATA');
	}
    public function transportasi() {
		return $this->hasMany('App\\TransportasiDet', 'KD_PAKETWISATA');
	}
    public function aktifitas() {
		return $this->hasMany('App\\AktifitasDet', 'KD_PAKETWISATA');
	}
    public function inclusion() {
		return $this->hasMany('App\\InclusionDet', 'KD_PAKETWISATA');
	}
    public function obyekwisata() {
		return $this->hasMany('App\\DestinasiDetail', 'KD_PAKETWISATA');
	}
    public function jadwal() {
		return $this->hasMany('App\\Jadwal', 'KD_PAKETWISATA');
	}
}
