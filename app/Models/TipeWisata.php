<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipeWisata extends Model
{
    protected $table = 'tb_tipewisata';
    public $timestamps = false;
    protected $fillable = [
        'KETERANGAN'
    ];
    public function paketwisata() {
		return $this->hasMany('tb_paketwisata', 'KD_TIPEWISATA');
	}
}
