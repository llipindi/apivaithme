<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inclusion extends Model
{
    protected $table = 'tb_inclusion';
    public $timestamps = false;
    protected $fillable = [
        'KETERANGAN'
    ];
    public function aktifitasdetail() {
		return $this->hasMany('tb_inclusiondet', 'KD_INCLUSION');
	}
}
