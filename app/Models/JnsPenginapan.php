<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JnsPenginapan extends Model
{
    protected $table = 'tb_jnspenginapan';
    public $timestamps = false;
    protected $fillable = [
        'NAMA_JENISPENGINAPAN','DESKRIPSI_JENISPENGINAPAN'
    ];
    public function penginapan() {
		return $this->hasMany('tb_penginapan', 'KD_JENISPENGINAPAN');
	}
}
