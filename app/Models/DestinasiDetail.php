<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DestinasiDetail extends Model
{
    protected $table = 'tb_detdestinasi';
    public $timestamps = false;
    protected $fillable = [
        'KD_PAKETWISATA','KD_OBYEKWISATADET'
    ];
    public function paketwisata() {
		return $this->belongsTo('App\\PaketWisata', 'KD_PAKETWISATA');
    }
    public function obyekwisatadet() {
		return $this->belongsTo('tb_obyekwisatadet', 'KD_OBYEKWISATADET');
	}
}
