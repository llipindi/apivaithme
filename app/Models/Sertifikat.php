<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sertifikat extends Model
{
    protected $table = 'tb_sertifikat';
    public $timestamps = false;
    protected $fillable = [
        'KD_AKUN', 'URL_SERTIFIKAT', 'DOMESTIK',
    ];
    public function akun() {
		return $this->belongsTo('tb_akun', 'KD_AKUN');
    }
}
