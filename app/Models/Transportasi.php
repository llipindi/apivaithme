<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transportasi extends Model
{
    protected $table = 'tb_transportasi';
    public $timestamps = false;
    protected $fillable = [
        'KD_JENISTRANSPORTASI', 'KD_BIROTRAVEL', 'HARGA','FOTO_TRANSPORTASI', 'NAMA_TRANSPORTASI'
    ];
    protected $hidden = [
        'KD_JENISTRANSPORTASI'
    ];
    public function transportasi() {
		return $this->belongsTo('tb_birotravel', 'KD_BIROTRAVEL');
    }
    public function jnstransportasi() {
		return $this->belongsTo('tb_jnstransportasi', 'KD_JENISTRANSPORTASI');
    }
}
