<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObyekWisataMas extends Model
{
    protected $table = 'tb_obyekwisatamas';
    public $timestamps = false;
    protected $fillable = [
        'KD_DESTINASI','NAMA_OBYEKWISATA'
    ];
    public function destinasi() {
		return $this->belongsTo('tb_destinasi', 'KD_DESTINASI');
	}
}
