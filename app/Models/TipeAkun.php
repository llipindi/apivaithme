<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipeAkun extends Model
{
    protected $table = 'tb_tipeakun';
    public $timestamps = false;
    protected $hidden = [
        'KD_TIPEAKUN'
    ];
    public function akun() {
		return $this->belongsTo('tb_akun', 'KD_TIPEAKUN');
    }
}
