<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObyekWisataDetail extends Model
{
    protected $table = 'tb_obyekwisatadet';
    public $timestamps = false;
    protected $fillable = [
        'KD_OBYEKWISATAMAS','KD_BIROTRAVEL','FOTO_OBYEKWISATA','HARGA'
    ];
    public function destinasi() {
		return $this->belongsTo('tb_obyekwisatamas', 'KD_OBYEKWISATAMAS');
	}
    public function biro() {
		return $this->belongsTo('tb_birotravel', 'KD_BIROTRAVEL');
	}
}
