<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObyekWisata extends Model
{
    protected $table = 'tb_obyekwisatamas';
    public $timestamps = false;
    protected $fillable = [
        'KD_DESTINASI','NAMA_OBYEKWISATA'
    ];
    public function destinasi() {
		return $this->belongsTo('App\\Destinasi', 'KD_DESTINASI');
    }
}
