<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportasiDet extends Model
{
    protected $table = 'tb_transportasidet';
    public $timestamps = false;
    protected $fillable = [
        'KD_TRANSPORTASI','KD_PAKETWISATA'
    ];
    public function paketwisata() {
		return $this->belongsTo('App\\PaketWisata', 'KD_PAKETWISATA');
    }
}
