<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penginapan extends Model
{
    protected $table = 'tb_penginapan';
    public $timestamps = false;
    protected $fillable = [
        'KD_JENISPENGINAPAN', 'KD_BIROTRAVEL', 'HARGA','FOTO_PENGINAPAN', 'NAMA_PENGINAPAN'
    ];
    protected $hidden = [
        'KD_JENISPENGINAPAN'
    ];
    public function penginapan() {
		return $this->belongsTo('tb_birotravel', 'KD_BIROTRAVEL');
    }
    public function jnspenginapan() {
		return $this->belongsTo('tb_jnspenginapan', 'KD_JENISPENGINAPAN');
    }
}
