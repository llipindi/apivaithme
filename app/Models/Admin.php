<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Akun;

class Admin extends Model
{
    protected $table = 'tb_admin';
    public $timestamps = false;
    protected $fillable = [
        'KD_AKUN', 'NAMA_ADMIN', 'NIK_ADMIN',
    ];
    public function akun() {
		return $this->belongsTo('Akun', 'KD_AKUN');
    }
    protected $hidden = [
        'KD_ADMIN'
    ];
    
}
