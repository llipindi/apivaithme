<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RencanaPerjalanan extends Model
{
    protected $table = 'tb_rencanaperjalanan';
    public $timestamps = false;
    protected $fillable = [
        'KD_PAKETWISATA','KETERANGAN','HARIKE_'
    ];
}
