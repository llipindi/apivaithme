<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'tb_review';
    public $timestamps = false;
    protected $fillable = [
        'KD_WISATAWAN','KD_RESERVASI','RATING','REVIEW',
    ];
    public function paketwisata() {
		return $this->belongsTo('tb_paketwisata', 'KD_PAKETWISATA');
	}
}
