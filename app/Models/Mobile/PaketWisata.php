<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class PaketWisata extends Model
{
    protected $table = 'tb_paketwisata';
    protected $primaryKey = 'KD_PAKETWISATA';

    public $timestamps = false;

    protected $fillable = [
        'JUDUL_PAKET', 'DESKRIPSI_PAKET', 'TOTAL', 'KD_BIROTRAVEL', 'KD_DESTINASI', 'KD_PENGINAPAN', 'KD_TIPEWISATA', 'STATUS_PAKET', 'STATUS_PRAMUWISATA'
    ];

    public function TIPE_WISATA()
    {
        return $this->hasOne('App\Models\Mobile\TipeWisata', 'KD_TIPEWISATA', 'KD_TIPEWISATA');
    }

    public function DESTINASI()
    {
        return $this->hasOne('App\Models\Mobile\Destinasi', 'KD_DESTINASI', 'KD_DESTINASI');
    }

    public function PENGINAPAN()
    {
        return $this->hasOne('App\Models\Mobile\Penginapan', 'KD_PENGINAPAN', 'KD_PENGINAPAN');
    }

    public function BIROTRAVEL()
    {
        return $this->hasOne('App\Models\Mobile\BiroTravel', 'KD_BIROTRAVEL', 'KD_BIROTRAVEL');
    }

    public function TRANSPORTASI_WISATA()
    {
        return $this->hasMany('App\Models\Mobile\TransportasiWisata', 'KD_PAKETWISATA', 'KD_PAKETWISATA');
    }

    public function OBYEK_WISATA_PAKET()
    {
        return $this->hasMany('App\Models\Mobile\ObyekWisataPaket', 'KD_PAKETWISATA', 'KD_PAKETWISATA');
    }

    public function JADWAL()
    {
        return $this->hasMany('App\Models\Mobile\Jadwal', 'KD_PAKETWISATA', 'KD_PAKETWISATA')->where('STATUS_JADWAL', 'Aktif');
    }

    public function INCLUSION()
    {
        return $this->hasMany('App\Models\Mobile\InclusionDetail', 'KD_PAKETWISATA', 'KD_PAKETWISATA');
    }

    public function AKTIFITAS_WISATA()
    {
        return $this->hasMany('App\Models\Mobile\AktifitasWisata', 'KD_PAKETWISATA', 'KD_PAKETWISATA');
    }

    public function RENCANA_PERJALANAN()
    {
        return $this->hasMany('App\Models\Mobile\RencanaPerjalanan', 'KD_PAKETWISATA', 'KD_PAKETWISATA');
    }
}