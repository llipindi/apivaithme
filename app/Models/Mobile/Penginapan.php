<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Penginapan extends Model
{
    protected $table = 'tb_penginapan';
    protected $primaryKey = 'KD_PENGINAPAN';

    public $timestamps = false;

    protected $appends = ['JENIS_PENGINAPAN'];

    protected $fillable = [
        'KD_JENISPENGINAPAN', 'KD_BIROTRAVEL', 'HARGA', 'FOTO_PENGINAPAN'
    ];

    public function getJenisPenginapanAttribute()
    {
        return $this->hasOne('App\Models\Mobile\JenisPenginapan', 'KD_JENISPENGINAPAN', 'KD_JENISPENGINAPAN')->first();
    }
}