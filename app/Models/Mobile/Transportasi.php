<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Transportasi extends Model
{
    protected $table = 'tb_transportasi';
    protected $primaryKey = 'KD_TRANSPORTASI';

    public $timestamps = false;

    protected $appends = ['JENIS_TRANSPORTASI'];

    protected $fillable = [
        'KD_JENISTRANSPORTASI', 'KD_BIROTRAVEL', 'HARGA', 'FOTO_TRANSPORTASI'
    ];

    public function getJenisTransportasiAttribute()
    {
        return $this->hasOne('App\Models\Mobile\JenisTransportasi', 'KD_JENISTRANSPORTASI', 'KD_JENISTRANSPORTASI')->first();
    }
}