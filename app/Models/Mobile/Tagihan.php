<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Tagihan extends Model
{
    protected $table = 'tb_tagihan';
    protected $primaryKey = 'KD_TAGIHAN';

    public $timestamps = false;

    protected $fillable = [
        'KD_RESERVASI', 'KD_ADMIN', 'STATUS_TAGIHAN', 'FOTO_TRANSFER', 'TGL_UPDATE'
    ];
}