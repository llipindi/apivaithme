<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class PemanduWisata extends Model
{
    protected $table = 'tb_pemanduwisata';
    protected $primaryKey = 'KD_PEMANDUWISATA';

    public $timestamps = false;

    protected $appends = ['AKUN'];

    protected $fillable = [
        'KD_PEMANDUWISATA',
        'KD_AKUN',
        'NAMA_PEMANDUWISATA',
        'NIK_PEMANDUWISATA',
        'JK_PEMANDUWISATA',
        'TL_PEMANDUWISATA',
        'NOTELP_PEMANDUWISATA',
        'PENDIDIKAN',
        'TGL_UPDATE'
    ];

    public function getAkunAttribute()
    {
        return $this->hasOne('App\Models\Mobile\Akun', 'KD_AKUN', 'KD_AKUN')->first();
    }
}