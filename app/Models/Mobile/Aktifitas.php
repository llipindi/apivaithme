<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Aktifitas extends Model
{
    protected $table = 'tb_aktifitas';
    protected $primaryKey = 'KD_AKTIFITAS';

    protected $fillable = [
        'NAMA_AKTIFITAS'
    ];
}
