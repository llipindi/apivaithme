<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class TipeWisata extends Model
{
    protected $table = 'tb_tipewisata';
    protected $primaryKey = 'KD_TIPEWISATA';

    public $timestamps = false;

    protected $fillable = [
        'KETERANGAN'
    ];
}