<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class BiroTravel extends Model
{
    protected $table = 'tb_birotravel';
    protected $primaryKey = 'KD_BIROTRAVEL';

    public $timestamps = false;

    protected $appends = ['AKUN'];

    protected $fillable = [
        'KD_AKUN', 'NAMA_BIRO', 'ALAMAT_BIRO', 'NOTELP_BIRO', 'NAMAPJ_BIRO', 'NIKPJBIRO', 'EMAILPJ_BIRO', 'JABATANPJBIRO', 'TERM', 'TGL_UPDATE'
    ];

    public function getAkunAttribute()
    {
        return $this->hasOne('App\Models\Mobile\Akun', 'KD_AKUN', 'KD_AKUN')->first();
    }
}