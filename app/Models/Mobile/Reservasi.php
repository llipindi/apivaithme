<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Reservasi extends Model
{
    protected $table = 'tb_reservasi';
    protected $primaryKey = 'KD_RESERVASI';

    public $timestamps = false;

    protected $fillable = [
        'KD_WISATAWAN',
        'KD_PEMANDUWISATA',
        'KD_PAKETWISATA',
        'TGL_PERJALANAN',
        'TOTAL_PAX',
        'TOTAL_PAKET',
        'TOTAL_PRAMUWISATA',
        'TOTAL_TAGIHAN',
        'TGL_RESERVASI',
        'TGL_UPDATE',
        'STATUS_RESERVASI',
        'RENCANA_PERJALANAN',
        'DURASI',
        'RESERVASI_ID'
    ];

    public function WISATAWAN()
    {
        return $this->hasOne('App\Models\Mobile\Wisatawan', 'KD_WISATAWAN', 'KD_WISATAWAN');
    }

    public function PEMANDU_WISATA()
    {
        return $this->hasOne('App\Models\Mobile\PemanduWisata', 'KD_PEMANDUWISATA', 'KD_PEMANDUWISATA');
    }

    public function PAKET_WISATA_LIST()
    {
        return $this->hasOne('App\Models\Mobile\PaketWisata', 'KD_PAKETWISATA', 'KD_PAKETWISATA')->with(['obyek_wisata_paket']);
    }

    public function PAKET_WISATA_DETAIL()
    {
        return $this->hasOne('App\Models\Mobile\PaketWisata', 'KD_PAKETWISATA', 'KD_PAKETWISATA')->with(['tipe_wisata', 'destinasi', 'obyek_wisata_paket', 'penginapan', 'transportasi_wisata', 'birotravel', 'jadwal', 'inclusion']);
    }

    public function REVIEW()
    {
        return $this->hasOne('App\Models\Mobile\Review', 'KD_RESERVASI', 'KD_RESERVASI');
    }
}