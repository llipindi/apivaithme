<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class JenisTransportasi extends Model
{
    protected $table = 'tb_jnstransportasi';
    protected $primaryKey = 'KD_JENISTRANSPORTASI';

    public $timestamps = false;

    protected $fillable = [
        'NAMA_JENISTRANSPORTASI', 'DESKRIPSI_JENISTRANSPORTASI'
    ];
}
