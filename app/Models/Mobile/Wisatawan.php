<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Wisatawan extends Model
{
    protected $table = 'tb_wisatawan';
    protected $primaryKey = 'KD_WISATAWAN';

    public $timestamps = false;

    protected $appends = ['AKUN'];

    protected $fillable = [
        'KD_AKUN', 'NAMA_WISATAWAN', 'TL_WISATAWAN', 'NOTELP_WISATAWAN', 'JK_WISATAWAN', 'ALAMAT_WISATAWAN', 'TGL_UPDATE'
    ];

    public function getAkunAttribute()
    {
        return $this->hasOne('App\Models\Mobile\Akun', 'KD_AKUN', 'KD_AKUN')->first();
    }
}