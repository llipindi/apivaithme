<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Review extends Model
{
    protected $table = 'tb_review';
    protected $primaryKey = 'KD_REVIEW';

    public $timestamps = false;

    protected $fillable = [
        'KD_WISATAWAN', 'KD_RESERVASI', 'RATING', 'REVIEW', 'TGL_UPDATE'
    ];
}