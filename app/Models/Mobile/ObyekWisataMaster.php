<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class ObyekWisataMaster extends Model
{
    protected $table = 'tb_obyekwisatamas';
    protected $primaryKey = 'KD_OBYEKWISATAMAS';

    public $timestamps = false;

    protected $appends = ['DESTINASI'];

    protected $fillable = [
        'KD_DESTINASI', 'NAMA_OBYEKSIWATA'
    ];

    public function getDestinasiAttribute()
    {
        return $this->hasOne('App\Models\Mobile\Destinasi', 'KD_DESTINASI', 'KD_DESTINASI')->first();
    }
}
