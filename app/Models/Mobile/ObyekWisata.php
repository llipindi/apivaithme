<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class ObyekWisata extends Model
{
    protected $table = 'tb_obyekwisatadet';
    protected $primaryKey = 'KD_OBYEKWISATADET';

    public $timestamps = false;

    protected $appends = ['OBYEK_WISATA_MASTER'];

    protected $fillable = [
        'KD_OBYEKWISATAMAS', 'KD_BIROTRAVEL', 'HARGA', 'FOTO_OBYEKWISATA'
    ];

    public function getObyekWisataMasterAttribute()
    {
        return $this->hasOne('App\Models\Mobile\ObyekWisataMaster', 'KD_OBYEKWISATAMAS', 'KD_OBYEKWISATAMAS')->first();
    }
}