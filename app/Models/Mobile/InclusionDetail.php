<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class InclusionDetail extends Model
{
    protected $table = 'tb_inclusiondet';
    protected $primaryKey = 'KD_INCLUSIONDET';

    public $timestamps = false;

    protected $appends = ['INCLUSION'];

    protected $fillable = [
        'KD_INCLUSION', 'KD_PAKETWISATA'
    ];

    public function getInclusionAttribute()
    {
        return $this->hasOne('App\Models\Mobile\Inclusion', 'KD_INCLUSION', 'KD_INCLUSION')->first();
    }
}