<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Destinasi extends Model
{
    protected $table = 'tb_destinasi';
    protected $primaryKey = 'KD_DESTINASI';

    public $timestamps = false;

    protected $fillable = [
        'NAMA_DESTINASI', 'HARGA_PRAMUWISATA'
    ];
}