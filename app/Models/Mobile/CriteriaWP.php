<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class CriteriaWP extends Model
{
    protected $table = 'tb_bobot_wp';

    protected $fillable = [
        // TYPE, 1 BENEFIT | 2 COST
        'cid', 'kriteria', 'bobot', 'perbaikan_bobot', 'type'
    ];
}