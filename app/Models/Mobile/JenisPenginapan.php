<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class JenisPenginapan extends Model
{
    protected $table = 'tb_jnspenginapan';
    protected $primaryKey = 'KD_JENISPENGINAPAN';

    public $timestamps = false;

    protected $fillable = [
        'NAMA_JENISPENGINAPAN', 'DESKRIPSI_JENISPENGINAPAN'
    ];
}
