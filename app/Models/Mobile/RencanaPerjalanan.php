<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class RencanaPerjalanan extends Model
{
    protected $table = 'tb_rencanaperjalanan';
    protected $primaryKey = 'KD_RENCANAPER';

    public $timestamps = false;

    protected $fillable = [
        'KD_RESERVASI', 'KETERANGAN', 'HARIKE_'
    ];
}