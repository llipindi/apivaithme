<?php

namespace App\Models\Mobile;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Jadwal extends Model
{
    protected $table = 'tb_jadwal';
    protected $primaryKey = 'KD_JADWAL';

    public $timestamps = false;

    protected $hidden = ['STATUS_JADWAL'];

    protected $appends = ['DURATION'];

    protected $fillable = [
        'TGL_MULAI', 'TGL_BERAKHIR', 'KD_PAKETWISATA', 'STATUS_JADWAL', 'SISA_KUOTA'
    ];

    public function getDurationAttribute()
    {
        $start = Carbon::parse($this->TGL_MULAI);
        $end = Carbon::parse($this->TGL_BERAKHIR);
        return $end->diffInDays($start) + 1;
    }
}