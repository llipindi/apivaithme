<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class AktifitasWisata extends Model
{
    protected $table = 'tb_aktifitasdetail';
    protected $primaryKey = 'KD_AKTIFITASDET';

    public $timestamps = false;

    protected $appends = ['AKTIFITAS'];

    protected $fillable = [
        'KD_AKTIFITAS', 'KD_PAKETWISATA'
    ];

    public function getAktifitasAttribute()
    {
        return $this->hasOne('App\Models\Mobile\Aktifitas', 'KD_AKTIFITAS', 'KD_AKTIFITAS')->first();
    }
}