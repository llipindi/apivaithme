<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Inclusion extends Model
{
    protected $table = 'tb_inclusion';
    protected $primaryKey = 'KD_INCLUSION';

    public $timestamps = false;

    protected $fillable = [
        'KETERANGAN'
    ];
}