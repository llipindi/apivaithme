<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class ObyekWisataPaket extends Model
{
    protected $table = 'tb_detdestinasi';
    protected $primaryKey = 'KD_DETDESTINASI';

    public $timestamps = false;

    protected $appends = ['OBYEK_WISATA'];

    protected $fillable = [
        'KD_OBYEKWISATADET', 'KD_PAKETWISATA'
    ];

    public function getObyekWisataAttribute()
    {
        return $this->hasOne('App\Models\Mobile\ObyekWisata', 'KD_OBYEKWISATADET', 'KD_OBYEKWISATADET')->first();
    }
}