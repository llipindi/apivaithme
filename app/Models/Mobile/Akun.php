<?php

namespace App\Models\Mobile;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Akun extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'tb_akun';
    protected $primaryKey = 'KD_AKUN';

    public $timestamps = false;

    protected $hidden = ['USERNAME', 'PASSWORD', 'KD_TIPEAKUN', 'STATUS', 'TGL_UPDATE'];

    protected $fillable = [
        'EMAIL', 'USERNAME', 'PASSWORD', 'KD_TIPEAKUN', 'FOTO', 'STATUS', 'TGL_UPDATE'
    ];

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->PASSWORD;
    }

    public function getAuthEmail()
    {
        return $this->EMAIL;
    }

    public function WISATAWAN()
    {
        return $this->hasOne('App\Models\Mobile\Wisatawan', 'KD_AKUN', 'KD_AKUN');
    }

    public function BIRO_TRAVEL()
    {
        return $this->hasOne('App\Models\Mobile\Birotravel', 'KD_AKUN', 'KD_AKUN');
    }

    public function PEMANDU_WISATA()
    {
        return $this->hasOne('App\Models\Mobile\PemanduWisata', 'KD_AKUN', 'KD_AKUN');
    }
}
