<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class TransportasiWisata extends Model
{
    protected $table = 'tb_transportasidet';
    protected $primaryKey = 'KD_TRANSPORTASIDET';

    public $timestamps = false;

    protected $appends = ['TRANSPORTASI'];

    protected $fillable = [
        'KD_TRANSPORTASI', 'KD_PAKETWISATA'
    ];

    public function getTransportasiAttribute()
    {
        return $this->hasOne('App\Models\Mobile\Transportasi', 'KD_TRANSPORTASI', 'KD_TRANSPORTASI')->first();
    }
}