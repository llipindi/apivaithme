<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aktifitas extends Model
{
    protected $table = 'tb_aktifitas';
    public $timestamps = false;
    protected $fillable = [
        'NAMA_AKTIFITAS'
    ];
    public function aktifitasdetail() {
		return $this->hasMany('tb_aktifitasdetail', 'KD_AKTIFITAS');
	}
}
