<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservasi extends Model
{
    protected $table = 'tb_reservasi';
    public $timestamps = false;
    protected $fillable = [
        'KD_WISATAWAN', 'KD_PEMANDUWISATA', 'KD_PAKETWISATA','TGL_PERJALANAN','TOTAL_PAX','TOTAL_TAGIHAN','TGL_RESERVASI','STATUS_RESERVASI','RENCANA_PERJALANAN','TOTAL_PAKET','TOTAL_PRAMUWISATA','DURASI','RESERVASI_ID','TGL_UPDATE'
    ];
    public function wisatawan() {
		return $this->belongsTo('tb_wisatawan', 'KD_WISATAWAN');
    }
    public function pramuwisata() {
		return $this->belongsTo('tb_pemanduwisata', 'KD_PEMANDUWISATA');
    }
    public function paketwisata() {
		return $this->belongsTo('tb_paketwisata', 'KD_PAKETWISATA');
    }
    public function jadwal() {
		return $this->belongsTo('tb_jadwal', 'KD_JADWAL');
    }
}
