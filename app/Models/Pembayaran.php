<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'tb_tagihan';
    public $timestamps = false;
    protected $fillable = [
        'KD_RESERVASI','KD_ADMIN','STATUS_TAGIHAN','FOTO_TRANSFER','KD_TAGIHAN','BANK','ATAS_NAMA','NO_REKENING'
    ];
    public function paketwisata() {
		return $this->belongsTo('tb_paketwisata', 'KD_PAKETWISATA');
	}
}
