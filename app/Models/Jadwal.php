<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'tb_jadwal';
    public $timestamps = false;
    protected $fillable = [
        'KD_PAKETWISATA','TGL_MULAI','TGL_BERAKHIR','STATUS_JADWAL','SISA_KUOTA'
    ];
    
    public function paketwisata() {
		return $this->belongsTo('App\\PaketWisata', 'KD_PAKETWISATA');
    }
}
