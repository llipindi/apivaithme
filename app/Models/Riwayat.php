<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Riwayat extends Model
{
    protected $table = 'tb_riwayatpemandu';
    public $timestamps = false;
    protected $fillable = [
        'KD_PEMANDUWISATA', 'KD_RESERVASI', 'STATUS'
    ];
}
