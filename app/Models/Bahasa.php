<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bahasa extends Model
{
    protected $table = 'tb_bahasapemandu';
    public $timestamps = false;
    protected $fillable = [
        'KD_AKUN','BAHASA','ENGLISH','LAINNYA'
    ];
}
