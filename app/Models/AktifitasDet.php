<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AktifitasDet extends Model
{
    protected $table = 'tb_aktifitasdetail';
    public $timestamps = false;
    protected $fillable = [
        'KD_AKTIFITAS','KD_PAKETWISATA'
    ];
    public function paketwisata() {
		return $this->belongsTo('App\\PaketWisata', 'KD_PAKETWISATA');
    }
}
