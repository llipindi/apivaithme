<?php
/**
 * Created by PhpStorm.
 * User: greytomato
 * Date: 4/4/17
 * Time: 11:10 AM
 */

/**
 *
 * FUNCTION FOR SAVE NEW IMAGE
 * @param $data
 * @param $error
 * @param $extra
 * @param null $success
 * @return \Illuminate\Http\JsonResponse $response
 */
function json_response($data, $code, $error_message = "", $error = false, $extra = null)
{
    $response_json = ["status" => true];
    $response = null;
    if ($error) {
        $response_json["status"] = false;
        $response_json["message"] = $error_message;
        $response = response()->json($response_json, $code);
    } else {
        $response_json["data"] = $data;
        if ($extra != null) {
            $response_json["extra"] = $extra;
        }
        $response = response()->json($response_json, 200);
    }
    $response->header('Content-Type', 'application/json');
    $response->header('charset', 'utf-8');
    return $response;
}

/**
 * SERVER KEY LARAVEL
 * @return string Server Key
 */
function server_key()
{
    // SET TIMEZONE
    date_default_timezone_set('Asia/Jakarta');
    return md5(date('d-m-Y'));
}

/**
 * IP MATCHING FUNCTION
 * @param $ip
 * @param $cidr
 * @return bool, true if match, false if not match
 */
function cidr_match($ip, $cidr)
{
    list($subnet, $mask) = explode('/', $cidr);
    if ((ip2long($ip) & ~((1 << (32 - $mask)) - 1)) == ip2long($subnet)) {
        return true;
    }
    return false;
}

/**
 * IMAGE PATH
 * @param $type , directory type
 * @return String, image path
 */
function image_path($type)
{
    return 'http://api.vaithme.com/images/' . $type . '/';
}