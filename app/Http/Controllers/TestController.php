<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Destinasi;
use App\Models\ObyekWisata;
use Illuminate\View\View;

class TestController extends Controller
{
    // fungsi menambahkan destinasi admin
    public function index(Request $request)
    {
        $data = ["tes" => "tes"];
        return view('firebase-client', $data);
    }
}