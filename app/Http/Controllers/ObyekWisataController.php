<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ObyekWisata;
use App\Models\ObyekWisataDetail;
use App\Models\BiroTravel;
use App\Models\Destinasi;
use App\Models\DestinasiDetail;
use PDO;
class ObyekWisataController extends Controller
{
     // fungsi menambahkan obyek wisata admin
     public function store(Request $request)
     {
         $this->validate(
             $request,[
                 'ObyekWisata'              => 'required',
                 'Kd_destinasi'              => 'required',
             ]
             );
             $obyek = ObyekWisata::create(
             [
                'KD_DESTINASI'           =>$request->Kd_destinasi,
                'NAMA_OBYEKWISATA'       =>$request->ObyekWisata,
             ]
             );
         if($obyek)
         {
             $res['status'] = "Success";
             $res['pesan']= "Data telah sukses ditambahkan";
             return response($res,201);
         }
         else
         {
             $res['status'] = "Success";
             $res['pesan']= "Data gagal ditambahkan";
             return response($res,200);
         }
     }
     // fungsi mengubah obyek wisata admin
     public function update(Request $request)
     {
         $this->validate(
             $request,[
                 'Kd_destinasi'         => 'required',
                 'ObyekWisata'          => 'required',
             ]
             );
         $obyek = ObyekWisata::where('KD_OBYEKWISATAMAS',$request->Kd_obyekwisata)->update(['NAMA_OBYEKWISATA'=>$request->ObyekWisata, 'KD_DESTINASI'=>$request->Kd_destinasi]);
         if ($obyek) 
         {
             $res['status'] = "Success";
             $res['pesan']= "Data telah berhasil diubah";
             return response($res,201);
         }
         else
         {
             $res['status'] = "Success";
             $res['pesan']= "tidak ada data yang diubah";
             return response($res,200);
         }
 
     }
     // fungsi menampilkan obyek wisata 
     public function index(Request $request)
     {
        $this->validate(
            $request,[
                'Kd_destinasi'                => 'required',
            ]
            );
         $obyek = ObyekWisata::where('KD_DESTINASI', $request->Kd_destinasi)->get();
 
         if(count($obyek) > 0){ //mengecek apakah data kosong atau tidak
             $res['status'] = "Success";
             $res['data'] = $obyek;
             return response($res);
         }
         else{
             $res['status'] = "Success";
             $res['data'] = "Data yang diminta tidak ada";
             return response($res,200);
         }
     }
    // fungsi menambahkan obyek wisata biro travel
    public function storeObyekWisataDet(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_obyekmaster'                => 'required',
                'Kd_akun'                       => 'required',
                'Harga'                         => 'required',
                'Foto'                          => 'required',
            ]
            );
            $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
            $obyek = ObyekWisataDetail::create(
            [
                'KD_OBYEKWISATAMAS'          =>$request->Kd_obyekmaster,
                'KD_BIROTRAVEL'              =>$biro->KD_BIROTRAVEL,
                'HARGA'                      =>$request->Harga,
                'FOTO_OBYEKWISATA'           =>$request->Foto,
            ]
            );
        if($obyek)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    public function ObyekWisataDet($id, $des)
    {
        $obyek = ObyekWisataDetail::Join('tb_obyekwisatamas', 'tb_obyekwisatamas.KD_OBYEKWISATAMAS', '=','tb_obyekwisatadet.KD_OBYEKWISATAMAS')
        ->select('tb_obyekwisatadet.KD_OBYEKWISATADET', 'tb_obyekwisatamas.NAMA_OBYEKWISATA','tb_obyekwisatadet.HARGA', 'tb_obyekwisatadet.FOTO_OBYEKWISATA')
        ->where('tb_obyekwisatadet.KD_BIROTRAVEL', $id)
        ->where('KD_DESTINASI', $des)
        ->get();
        return $obyek;
        // echo $obyek;
    }
    // fungsi menampilkan obyek wisata biro travel
    public function indexObyekWisataDet(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'      => 'required',
            ]
            );
        $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
        $destinasi = Destinasi::all();
        $result = array();
        foreach($destinasi as $row)
        {
            $row['Obyek Wisata']=$this->ObyekWisataDet($biro->KD_BIROTRAVEL,$row['KD_DESTINASI']);
            array_push($result,$row);
        }
        if(count($result) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] =$result;
            return response($res);
            
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi mengubah obyek wisata biro travel
    public function updateObyekWisataDet(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_obyekdet'                   => 'required',
                'Harga'                         => 'required',
                'Foto'                          => 'required',
            ]
            );
        $obyek = ObyekWisataDetail::where('KD_OBYEKWISATADET',$request->Kd_obyekdet)
        ->update(['HARGA'=>$request->Harga, 'FOTO_OBYEKWISATA'=>$request->Foto]);
        if ($obyek) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }
    }
    public function listobyek($id, $des)
    {
        $query = ObyekWisataDetail::select('KD_OBYEKWISATAMAS')->where('tb_obyekwisatadet.KD_BIROTRAVEL', $id)->from('tb_obyekwisatadet');
        $obyek = ObyekWisata::Select('*')->where('KD_DESTINASI', $des)->WhereNotIn('KD_OBYEKWISATAMAS',$query)->get();
        return $obyek;
    }
    // fungsi menampilkan list obyek wisata master untuk mengisi detail
    public function indexlistobyekwisatadet(Request $request)
    {
        $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
        $destinasi = Destinasi::all();
        $result = array();
        foreach($destinasi as $row)
        {
            $row['Obyek Wisata']=$this->listobyek($biro->KD_BIROTRAVEL,$row['KD_DESTINASI']);
            array_push($result,$row);
        }
        if(count($result) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $result;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    
    // menghapus obyek master biro travel
    public function delete(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_obyek'                 => 'required',
            ]
            );
        $cari = ObyekWisataDetail::where('KD_OBYEKWISATAMAS',$request->Kd_obyek)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $obyek = ObyekWisata::where('KD_OBYEKWISATAMAS',$request->Kd_obyek)->delete();
            // $penginapan->delete();
            if($obyek)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,200);
            }
        }
    }
    // menghapus obyek detail biro travel
    public function deletejns(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_obyek'                 => 'required',
            ]
            );
        $cari = DestinasiDetail::where('KD_OBYEKWISATADET',$request->Kd_obyek)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $obyek = ObyekWisataDet::where('KD_OBYEKWISATADET',$request->Kd_obyek)->delete();
            if($obyek)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,200);
            }
        }
    }
    // fungsi menambahkan destinasi detail
    // public function storedestinasidet(Request $request)
    // {
    //     $transportasi=DB::table('tb_detdestinasi')->insert($request['ObyekWisata']);

    //     if($transportasi )
    //     { 
    //         $res['status'] = "Success";
    //         $res['data'] = "Data berhasil ditambahkan";
    //         return response($res);
    //     }
    //     else{
    //         $res['status'] = "Success";
    //         $res['data'] = "Data gagal ditambahkan";
    //         return response($res,200);
    //     }
    // }

}
