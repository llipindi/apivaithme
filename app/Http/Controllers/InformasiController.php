<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Informasi;

class InformasiController extends Controller
{
    public function index(Request $request)
    {
        $informasi = Informasi::all();
        $res['status'] = "Success";
        $res['data'] = $informasi;
        return response($res,200);

    }
    //fungsi mengubah rekening
    public function update(Request $request)
    {
        $this->validate(
            $request,[
                'Kontak_Telp'           => 'required',
                'Kontak_instagram'      => 'required',
                'Kontak_facebook'       => 'required',
                'Kontak_email'          => 'required',
                'Alamat'                => 'required',
                'Kode'                => 'required',
            ]
            );
        $rekening = Informasi::where('KODE',$request->Kode)->update(['KONTAK_TELP'=>$request->Kontak_Telp, 'KONTAK_INSTAGRAM'=>$request->Kontak_instagram, 'KONTAK_EMAIL'=>$request->Kontak_facebook, 'ALAMAT'=>$request->Alamat]);
        if ($rekening) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
        }
        else
        {
            {
                $res['status'] = "Success";
                $res['pesan']= "tidak ada data yang diubah";
                return response($res,200);
            }
        }

    }
}
