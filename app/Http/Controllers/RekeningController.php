<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rekening;
use Auth;

class RekeningController extends Controller
{
    // fungsi menambahkan rekening
    public function store(Request $request)
    {
        $this->validate(
            $request,[
                'Bank'              => 'required',
                'Nama_rekening'     => 'required',
                'No_rekening'       => 'required',
                'Kd_akun'           => 'required'
            ]
            );
            $rekening = Rekening::create(
            [
                'KD_AKUN'       =>$request->Kd_akun,
                'NO_REKENING'   =>$request->No_rekening,
                'BANK'          =>$request->Bank,
                'NAMA_REKENING' =>$request->Nama_rekening,
            ]
            );
        if($rekening)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkans";
            return response($res,200);
        }
    }
    // fungsi melihat rekening
    public function index(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'   => 'required'
            ]
            );
        $rekening = Rekening::where('KD_AKUN', $request->Kd_akun)->get();
        $res['status'] = "Success";
        $res['data'] = $rekening;
        return response($res,200);

    }
    //fungsi mengubah rekening
    public function update(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_rekening'     => 'required',
                'No_rekening'     => 'required',
                'Bank'            => 'required',
                'Nama_rekening'   => 'required',
            ]
            );
        $rekening = Rekening::where('KD_REKENING',$request->Kd_rekening)->update(['NO_REKENING'=>$request->No_rekening, 'BANK'=>$request->Bank, 'NAMA_REKENING'=>$request->Nama_rekening]);
        if ($rekening) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
        }
        else
        {
            {
                $res['status'] = "Success";
                $res['pesan']= "tidak ada data yang diubah";
                return response($res,200);
            }
        }

    }

}
