<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Destinasi;
use App\Models\ObyekWisata;

class DestinasiController extends Controller
{
    // fungsi menambahkan destinasi admin
    public function store(Request $request)
    {
        $this->validate(
            $request,[
                'Destinasi'             => 'required',
            ]
            );
            $destinasi = Destinasi::create(
            [
                'NAMA_DESTINASI'        => $request->Destinasi,
                'HARGA_PRAMUWISATA'	=> $request->Harga,
            ]
            );
        if($destinasi)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi mengubah destinasi admin
    public function update(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_destinasi'      => 'required',
                'Destinasi'         => 'required',
            ]
            );
        $destinasi = Destinasi::where('KD_DESTINASI',$request->Kd_destinasi)->update(['NAMA_DESTINASI'=>$request->Destinasi,'HARGA_PRAMUWISATA'=>$request->Harga]);
        if ($destinasi) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
    // fungsi menampilkan destinasi
    public function index()
    {
        $destinasi = Destinasi::all();

        if(count($destinasi) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $destinasi;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
        // menghapus obyek detail biro travel
    public function delete(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_destinasi'                 => 'required',
            ]
            );
        $cari = ObyekWisata::where('KD_DESTINASI',$request->Kd_destinasi)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $obyek = Destinasi::where('KD_DESTINASI',$request->Kd_destinasi)->delete();
            if($obyek)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,204);
            }
        }
    }
    
}
