<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaketWisata;
use App\Models\BiroTravel;
use App\Models\AktifitasDet;
use App\Models\InclusionDet;
use App\Models\Jadwal;
use App\Models\TransportasiDet;
use App\Models\DestinasiDetail;
use App\Models\Review;
use App\Models\Reservasi;
use App\Models\PemanduWisata;
use App\Models\Riwayat;
use App\Models\RencanaPerjalanan;
use App\Models\SaldoBiro;
use App\Models\SaldoPemandu;
use App\Models\Sertifikat;
use App\Models\ReviewPemandu;
use App\Models\Pembayaran;
use App\Models\Pengembalian;
use App\Models\Admin;
use DateTime;
use Carbon\Carbon;
class ReservasiController extends Controller
{
    public function obyekwisata($paket)
    {
        $obyek = DestinasiDetail::Join('tb_obyekwisatadet', 'tb_detdestinasi.KD_OBYEKWISATADET', '=','tb_obyekwisatadet.KD_OBYEKWISATADET')
        ->Join('tb_obyekwisatamas', 'tb_obyekwisatamas.KD_OBYEKWISATAMAS', '=','tb_obyekwisatadet.KD_OBYEKWISATAMAS')
        ->select('tb_obyekwisatadet.KD_OBYEKWISATADET', 'tb_obyekwisatamas.NAMA_OBYEKWISATA','tb_obyekwisatadet.HARGA', 'tb_obyekwisatadet.FOTO_OBYEKWISATA')
        ->where('KD_PAKETWISATA', $paket)
        ->get();
        return $obyek;
    }
    public function rencana($paket)
    {
        $obyek = RencanaPerjalanan::Join('tb_reservasi', 'tb_rencanaperjalanan.KD_PAKETWISATA', '=','tb_reservasi.KD_PAKETWISATA')
        ->select('tb_rencanaperjalanan.KD_RENCANAPER','tb_rencanaperjalanan.KETERANGAN','tb_rencanaperjalanan.HARIKE_')
        ->where('tb_rencanaperjalanan.KD_PAKETWISATA', $paket)
        ->get();
        return $obyek;
    }
    public function transportasi($paket)
    {
        $transportasi = TransportasiDet::Join('tb_transportasi', 'tb_transportasidet.KD_TRANSPORTASI', '=','tb_transportasi.KD_TRANSPORTASI')
        ->Join('tb_jnstransportasi', 'tb_jnstransportasi.KD_JENISTRANSPORTASI', '=','tb_transportasi.KD_JENISTRANSPORTASI')
        ->select('tb_transportasi.NAMA_TRANSPORTASI', 'tb_transportasi.HARGA','tb_transportasi.FOTO_TRANSPORTASI', 'tb_jnstransportasi.NAMA_JENISTRANSPORTASI')
        ->where('KD_PAKETWISATA', $paket)
        ->get();
        return $transportasi;
    }
    public function jadwal($paket,$tgl)
    {
        $jadwal = Jadwal::where('KD_PAKETWISATA', $paket)->where('TGL_MULAI',$tgl)
        ->select('TGL_MULAI','TGL_BERAKHIR','STATUS_JADWAL')->get();
        return $jadwal;
    }
    public function review($paket)
    {
        $review = Review::Join('tb_reservasi', 'tb_review.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
        ->leftjoin('tb_wisatawan', 'tb_reservasi.kd_wisatawan', '=','tb_wisatawan.kd_wisatawan')
        ->leftjoin('tb_akun', 'tb_akun.kd_akun', '=','tb_wisatawan.kd_akun')
        ->select('tb_reservasi.RESERVASI_ID', 'tb_review.RATING','tb_review.REVIEW','tb_wisatawan.NAMA_WISATAWAN','tb_akun.FOTO')
        ->where('KD_PAKETWISATA', $paket)
        ->get();
        return $review;
    }
    public function reviewpemandu($paket)
    {
        $review = ReviewPemandu::Join('tb_reservasi', 'tb_reviewpemanduwisata.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
        ->leftjoin('tb_wisatawan', 'tb_reservasi.kd_wisatawan', '=','tb_wisatawan.kd_wisatawan')
        ->leftjoin('tb_akun', 'tb_akun.kd_akun', '=','tb_wisatawan.kd_akun')
        ->select('tb_reservasi.RESERVASI_ID', 'tb_reviewpemanduwisata.RATING','tb_reviewpemanduwisata.REVIEW','tb_wisatawan.NAMA_WISATAWAN','tb_akun.FOTO')
        ->where('KD_PAKETWISATA', $paket)
        ->get();
        return $review;
    }
    public function rating($paket)
    {
        $rating = Review::Join('tb_reservasi', 'tb_review.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
        // ->Join('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
        ->select('tb_reservasi.RESERVASI_ID', 'tb_review.RATING','tb_review.REVIEW')
        ->where('KD_PAKETWISATA', $paket)
        ->groupBy('KD_PAKETWISATA')
        ->avg('RATING');
        return $rating;
    }
    //fungsi untuk menampilkan order
    public function index(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'      => 'required',
                'Tipe_akun'    => 'required',
                'Status'       => 'required',
            ]
            );
        if ($request->Tipe_akun == 'Biro Travel')
        {
            $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
            $reservasi = Reservasi::
            leftjoin('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
            ->leftjoin('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
            ->leftjoin('tb_penginapan', 'tb_penginapan.KD_PENGINAPAN', '=','tb_paketwisata.KD_PENGINAPAN')
            ->leftjoin('tb_tipewisata', 'tb_tipewisata.KD_TIPEWISATA', '=','tb_paketwisata.KD_TIPEWISATA')
            ->select('tb_reservasi.KD_RESERVASI','tb_reservasi.TOTAL_PAKET','tb_reservasi.TOTAL_PAX','tb_reservasi.TGL_PERJALANAN','tb_paketwisata.KD_PAKETWISATA', 'tb_paketwisata.JUDUL_PAKET','tb_paketwisata.DESKRIPSI_PAKET', 'tb_paketwisata.TOTAL', 'tb_paketwisata.STATUS_PRAMUWISATA'
            , 'tb_paketwisata.STATUS_PAKET', 'tb_destinasi.NAMA_DESTINASI', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_tipewisata.KETERANGAN','tb_paketwisata.TOTAL')
            ->where('tb_reservasi.STATUS_RESERVASI', $request->Status)
            ->where('tb_paketwisata.KD_BIROTRAVEL', $biro->KD_BIROTRAVEL)->get();
            $result = array();
            foreach($reservasi as $row)
            {
                $row['Total Biro Travel']=($row['TOTAL_PAKET']);
                $row['Obyek Wisata']=$this->obyekwisata($row['KD_PAKETWISATA']);
                $row['Transportasi']=$this->transportasi($row['KD_PAKETWISATA']);
                $row['Jadwal']=$this->jadwal($row['KD_PAKETWISATA'],$row['TGL_PERJALANAN']);
                $row['Review']=$this->review($row['KD_PAKETWISATA']);
                $rating=$this->rating($row['KD_PAKETWISATA']);
                $row['Rating']=number_format((float)$rating, 2, '.', '');
                $row['Jml Perjalanan'] = Reservasi::where('STATUS_RESERVASI','Finish')->where('KD_PAKETWISATA',$row['KD_PAKETWISATA'])->count();
                array_push($result,$row);
            }
        }
        else
        {
            $pemandu = PemanduWisata::where('KD_AKUN', $request->Kd_akun)->first();
            $reservasi = Reservasi::
            leftjoin('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
            ->leftjoin('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
            ->leftjoin('tb_penginapan', 'tb_penginapan.KD_PENGINAPAN', '=','tb_paketwisata.KD_PENGINAPAN')
            ->leftjoin('tb_tipewisata', 'tb_tipewisata.KD_TIPEWISATA', '=','tb_paketwisata.KD_TIPEWISATA')
            ->select('tb_reservasi.KD_RESERVASI','tb_reservasi.TOTAL_PAX','tb_reservasi.TGL_PERJALANAN','tb_reservasi.TOTAL_PRAMUWISATA','tb_paketwisata.KD_PAKETWISATA', 'tb_paketwisata.JUDUL_PAKET','tb_paketwisata.DESKRIPSI_PAKET', 'tb_paketwisata.TOTAL', 'tb_paketwisata.STATUS_PRAMUWISATA'
            , 'tb_paketwisata.STATUS_PAKET', 'tb_destinasi.NAMA_DESTINASI', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_tipewisata.KETERANGAN')
            ->where('tb_reservasi.STATUS_RESERVASI', $request->Status)
            ->where('tb_reservasi.KD_PEMANDUWISATA', $pemandu->KD_PEMANDUWISATA)
            //->where('tb_paketwisata.STATUS_PRAMUWISATA', 'YA')
            ->get();
            $result = array();
            foreach($reservasi as $row)
            {
                $row['Total Pramuwisata']=($row['TOTAL_PRAMUWISATA']);
                $row['Obyek Wisata']=$this->obyekwisata($row['KD_PAKETWISATA']);
                $row['Transportasi']=$this->transportasi($row['KD_PAKETWISATA']);
                $row['Jadwal']=$this->jadwal($row['KD_PAKETWISATA'],$row['TGL_PERJALANAN']);
                $row['Review']=$this->review($row['KD_PAKETWISATA']);
                $rating=$this->rating($row['KD_PAKETWISATA']);
                $row['Rating']=number_format((float)$rating, 2, '.', '');
                $row['Jml Perjalanan'] = Reservasi::where('STATUS_RESERVASI','5')->where('KD_PAKETWISATA',$row['KD_PAKETWISATA'])->count();
                array_push($result,$row);
            }
        }

        if(count($result) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] =$result;
            return response($res);          
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi melihat riwayat biro travel dan pemandu wisata
    public function riwayat_jadwal(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'      => 'required',
                'Tipe_akun'    => 'required',
            ]
            );
        if ($request->Tipe_akun == 'Biro Travel')
        {
            $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
            $reservasi = Reservasi::
            leftjoin('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
            ->leftjoin('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
            ->leftjoin('tb_penginapan', 'tb_penginapan.KD_PENGINAPAN', '=','tb_paketwisata.KD_PENGINAPAN')
            ->leftjoin('tb_tipewisata', 'tb_tipewisata.KD_TIPEWISATA', '=','tb_paketwisata.KD_TIPEWISATA')
            ->select('tb_reservasi.KD_RESERVASI','tb_reservasi.TOTAL_PAX','tb_reservasi.TGL_PERJALANAN','tb_paketwisata.KD_PAKETWISATA', 'tb_paketwisata.JUDUL_PAKET','tb_paketwisata.DESKRIPSI_PAKET', 'tb_reservasi.TOTAL_PAKET', 'tb_paketwisata.STATUS_PRAMUWISATA'
            , 'tb_paketwisata.STATUS_PAKET', 'tb_destinasi.NAMA_DESTINASI', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_tipewisata.KETERANGAN')
            // ->where('tb_reservasi.STATUS_RESERVASI', '5')
            ->Where('tb_reservasi.STATUS_RESERVASI', '7')
            ->where('tb_paketwisata.KD_BIROTRAVEL', $biro->KD_BIROTRAVEL)->get();
            $result = array();
            foreach($reservasi as $row)
            {
                $row['Total Biro travel']=($row['TOTAL_PAKET']);
                $row['Obyek Wisata']=$this->obyekwisata($row['KD_PAKETWISATA']);
                $row['Transportasi']=$this->transportasi($row['KD_PAKETWISATA']);
                $row['Jadwal']=$this->jadwal($row['KD_PAKETWISATA'],$row['TGL_PERJALANAN']);
                $row['Review']=$this->review($row['KD_PAKETWISATA']);
                $row['Jml Perjalanan'] = Reservasi::where('STATUS_RESERVASI','7')->where('KD_PAKETWISATA',$row['KD_PAKETWISATA'])->count();
               
                if ($row['STATUS_PRAMUWISATA'] == 'Ya')
                {
                    $row['Pramuwisata'] = Riwayat::Join('tb_pemanduwisata', 'tb_riwayatpemandu.KD_PEMANDUWISATA', '=','tb_pemanduwisata.KD_PEMANDUWISATA')
                    ->select('tb_pemanduwisata.NAMA_PEMANDUWISATA')
                    ->where('tb_riwayatpemandu.KD_RESERVASI',$row['KD_RESERVASI'])->first();
                }
                else
                {
                    $row['Pramuwisata'] = null;
                }
                array_push($result,$row);
            }
        }
        else
        {
            $pemandu = PemanduWisata::where('KD_AKUN', $request->Kd_akun)->first();
            $reservasi =  Reservasi::
            leftjoin('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
            ->leftjoin('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
            ->leftjoin('tb_penginapan', 'tb_penginapan.KD_PENGINAPAN', '=','tb_paketwisata.KD_PENGINAPAN')
            ->leftjoin('tb_tipewisata', 'tb_tipewisata.KD_TIPEWISATA', '=','tb_paketwisata.KD_TIPEWISATA')
            ->select('tb_reservasi.KD_RESERVASI','tb_reservasi.TOTAL_PAX','tb_reservasi.TGL_PERJALANAN','tb_reservasi.TOTAL_PRAMUWISATA','tb_paketwisata.KD_PAKETWISATA', 'tb_paketwisata.JUDUL_PAKET','tb_paketwisata.DESKRIPSI_PAKET', 'tb_paketwisata.TOTAL', 'tb_paketwisata.STATUS_PRAMUWISATA'
            , 'tb_paketwisata.STATUS_PAKET', 'tb_destinasi.NAMA_DESTINASI', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_tipewisata.KETERANGAN')
            ->where('tb_reservasi.STATUS_RESERVASI', '7')
            ->where('tb_reservasi.KD_PEMANDUWISATA', $pemandu->KD_PEMANDUWISATA)
            //Riwayat::
            //Join('tb_reservasi', 'tb_riwayatpemandu.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
            //->Join('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
            //->Join('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
            //->Join('tb_penginapan', 'tb_penginapan.KD_PENGINAPAN', '=','tb_paketwisata.KD_PENGINAPAN')
            //->Join('tb_tipewisata', 'tb_tipewisata.KD_TIPEWISATA', '=','tb_paketwisata.KD_TIPEWISATA')
            //->select('tb_reservasi.KD_RESERVASI','tb_reservasi.TOTAL_PAX','tb_reservasi.TOTAL_PRAMUWISATA','tb_reservasi.TGL_PERJALANAN','tb_paketwisata.KD_PAKETWISATA', 'tb_paketwisata.JUDUL_PAKET','tb_paketwisata.DESKRIPSI_PAKET', 'tb_paketwisata.TOTAL', 'tb_paketwisata.STATUS_PRAMUWISATA'
            //, 'tb_paketwisata.STATUS_PAKET', 'tb_destinasi.NAMA_DESTINASI', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_tipewisata.KETERANGAN')
            //->where('tb_reservasi.STATUS_RESERVASI', '7')
            //->where('tb_reservasi.KD_PEMANDUWISATA', $pemandu->KD_PEMANDUWISATA)
            //->where('tb_paketwisata.STATUS_PRAMUWISATA', 'YA')
            //->where('tb_riwayatpemandu.STATUS', 'TERIMA')
            //->orWhere('tb_riwayatpemandu.STATUS', 'TOLAK')
            ->get();
            $result = array();
            foreach($reservasi as $row)
            {
                $row['Total Pramuwisata ']=($row['TOTAL_PRAMUWISATA']);
                $row['Obyek Wisata']=$this->obyekwisata($row['KD_PAKETWISATA']);
                $row['Transportasi']=$this->transportasi($row['KD_PAKETWISATA']);
                $row['Jadwal']=$this->jadwal($row['KD_PAKETWISATA'],$row['TGL_PERJALANAN']);
                $row['Review']=$this->reviewpemandu($row['KD_PAKETWISATA']);
                $row['Jml Perjalanan'] = Reservasi::where('STATUS_RESERVASI','7')->where('KD_PAKETWISATA',$row['KD_PAKETWISATA'])->count();
                
                array_push($result,$row);
            }
        }
        if(count($result) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] =$result;
            return response($res);          
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    //fungsi melihat jadwal biro travel dan pemandu wisata
    public function jadwal_reservasi(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'      => 'required',
                'Tipe_akun'    => 'required',
                'Status'       => 'required',
            ]
            );
        if ($request->Tipe_akun == 'Biro Travel')
        {
            $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
            $reservasi = Reservasi::
            leftjoin('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
            ->leftjoin('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
            ->leftjoin('tb_penginapan', 'tb_penginapan.KD_PENGINAPAN', '=','tb_paketwisata.KD_PENGINAPAN')
            ->leftjoin('tb_tipewisata', 'tb_tipewisata.KD_TIPEWISATA', '=','tb_paketwisata.KD_TIPEWISATA')
            ->select('tb_reservasi.KD_RESERVASI','tb_reservasi.TOTAL_PAX','tb_reservasi.TGL_PERJALANAN','tb_paketwisata.KD_PAKETWISATA', 'tb_paketwisata.JUDUL_PAKET','tb_paketwisata.DESKRIPSI_PAKET', 'tb_reservasi.TOTAL_PAKET', 'tb_paketwisata.STATUS_PRAMUWISATA'
            , 'tb_paketwisata.STATUS_PAKET', 'tb_destinasi.NAMA_DESTINASI', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_tipewisata.KETERANGAN')
            ->where('tb_reservasi.STATUS_RESERVASI', $request->Status)
            ->where('tb_paketwisata.KD_BIROTRAVEL', $biro->KD_BIROTRAVEL)->get();
            $result = array();
            foreach($reservasi as $row)
            {
                $row['Obyek Wisata']=$this->obyekwisata($row['KD_PAKETWISATA']);
                $row['Transportasi']=$this->transportasi($row['KD_PAKETWISATA']);
                $row['Jadwal']=$this->jadwal($row['KD_PAKETWISATA'],$row['TGL_PERJALANAN']);
                $row['Rencana']=$this->rencana($row['KD_PAKETWISATA']);
                $row['Review']=$this->review($row['KD_PAKETWISATA']);
                $row['Jml Perjalanan'] = Reservasi::where('STATUS_RESERVASI','5')->where('KD_PAKETWISATA',$row['KD_PAKETWISATA'])->count();
                if ($row['STATUS_PRAMUWISATA'] == 'Ya')
                {
                    $row['Pramuwisata'] = Riwayat::Join('tb_pemanduwisata', 'tb_riwayatpemandu.KD_PEMANDUWISATA', '=','tb_pemanduwisata.KD_PEMANDUWISATA')
                    ->select('tb_pemanduwisata.NAMA_PEMANDUWISATA','tb_pemanduwisata.NOTELP_PEMANDUWISATA')
                    ->where('tb_riwayatpemandu.KD_RESERVASI',$row['KD_RESERVASI'])->first();
                }
                else
                {
                    $row['Pramuwisata'] = null;
                }
                $row['Wisatawan'] = Reservasi::Join('tb_wisatawan', 'tb_reservasi.KD_WISATAWAN', '=','tb_wisatawan.KD_WISATAWAN')
                    ->select('tb_wisatawan.NAMA_WISATAWAN','tb_wisatawan.NOTELP_WISATAWAN')
                    ->where('tb_reservasi.KD_RESERVASI',$row['KD_RESERVASI'])->first();
                array_push($result,$row);
            }
        }
        else if ($request->Tipe_akun == 'Pemandu Wisata')
        {
            $pemandu = PemanduWisata::where('KD_AKUN', $request->Kd_akun)->first();
            $reservasi =  Reservasi::
            leftjoin('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
            ->leftjoin('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
            ->leftjoin('tb_penginapan', 'tb_penginapan.KD_PENGINAPAN', '=','tb_paketwisata.KD_PENGINAPAN')
            ->leftjoin('tb_tipewisata', 'tb_tipewisata.KD_TIPEWISATA', '=','tb_paketwisata.KD_TIPEWISATA')
            ->select('tb_reservasi.KD_RESERVASI','tb_reservasi.TOTAL_PAX','tb_reservasi.TGL_PERJALANAN','tb_reservasi.TOTAL_PRAMUWISATA','tb_paketwisata.KD_PAKETWISATA', 'tb_paketwisata.JUDUL_PAKET','tb_paketwisata.DESKRIPSI_PAKET', 'tb_paketwisata.TOTAL', 'tb_paketwisata.STATUS_PRAMUWISATA'
            , 'tb_paketwisata.STATUS_PAKET', 'tb_destinasi.NAMA_DESTINASI', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_tipewisata.KETERANGAN')
            ->where('tb_reservasi.STATUS_RESERVASI', $request->Status)
            ->where('tb_reservasi.KD_PEMANDUWISATA', $pemandu->KD_PEMANDUWISATA)
            ->get();
            $result = array();
            foreach($reservasi as $row)
            {
               
                // $row['Total Harga']=($row['TOTAL_PRAMUWISATA']);
                $row['Obyek Wisata']=$this->obyekwisata($row['KD_PAKETWISATA']);
                $row['Transportasi']=$this->transportasi($row['KD_PAKETWISATA']);
                $row['Rencana']=$this->rencana($row['KD_PAKETWISATA']);
                $row['Jadwal']=$this->jadwal($row['KD_PAKETWISATA'],$row['TGL_PERJALANAN']);
                $row['Review']=$this->review($row['KD_PAKETWISATA']);
                $row['Jml Perjalanan'] = Reservasi::where('STATUS_RESERVASI','5')->where('KD_PAKETWISATA',$row['KD_PAKETWISATA'])->count();
                $row['Biro'] = Reservasi::Join('tb_paketwisata', 'tb_reservasi.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
                    ->Join('tb_birotravel', 'tb_paketwisata.KD_BIROTRAVEL', '=','tb_birotravel.KD_BIROTRAVEL')
                    ->select('tb_birotravel.NAMA_BIRO','tb_birotravel.NOTELP_BIRO')
                    ->where('tb_reservasi.KD_RESERVASI',$row['KD_RESERVASI'])->first();
                $row['Wisatawan'] = Reservasi::Join('tb_wisatawan', 'tb_reservasi.KD_WISATAWAN', '=','tb_wisatawan.KD_WISATAWAN')
                    ->select('tb_wisatawan.NAMA_WISATAWAN','tb_wisatawan.NOTELP_WISATAWAN')
                    ->where('tb_reservasi.KD_RESERVASI',$row['KD_RESERVASI'])->first();
                array_push($result,$row);
            }
        }
        if(count($result) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] =$result;
            return response($res);          
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }        
    }
    // fungsi untuk mengubah status reservasi
    public function update(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_reservasi'      => 'required',
                'Status'            => 'required',
            ]
            );
        if($request->Status == '7')
        {
            $reservasi = Reservasi::where('KD_RESERVASI',$request->Kd_reservasi)->update(['STATUS_RESERVASI'=>$request->Status]);
            $reservasidetail = Reservasi::where('KD_RESERVASI',$request->Kd_reservasi)->select('TOTAL_PAKET','TOTAL_PRAMUWISATA','KD_PEMANDUWISATA','KD_PAKETWISATA')->first();
            $biro=PaketWisata::where('KD_PAKETWISATA',$reservasidetail->KD_PAKETWISATA)->select('KD_BIROTRAVEL')->first();
            //echo $reservasidetail->TOTAL_PAKET;
            $saldobiro = SaldoBiro::create(
                [
                    'KD_BIROTRAVEL'         =>$biro->KD_BIROTRAVEL,
                    'TOTAL'                 =>$reservasidetail->TOTAL_PAKET,
                ]
                );
            $saldopramu = SaldoPemandu::create(
                [
                    'KD_PEMANDUWISATA'         =>$reservasidetail ->KD_PEMANDUWISATA,
                    'TOTAL'                    =>$reservasidetail->TOTAL_PRAMUWISATA,
                ]
                );
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
           
        }
        else if($request->Status == '2')
        {
        	
            $reservasi = Reservasi::where('KD_RESERVASI',$request->Kd_reservasi)->update(['STATUS_RESERVASI'=>$request->Status]);
            $pembayaran = Pembayaran::create([
                    'KD_RESERVASI'         	=>$request->Kd_reservasi,
                    'STATUS_TAGIHAN'      	=>'1'
                ]
                );
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
            
        }
        else
        {
            $reservasi = Reservasi::where('KD_RESERVASI',$request->Kd_reservasi)->update(['STATUS_RESERVASI'=>$request->Status]);
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
        }
        
        //if ($reservasi && $saldobiro && $saldopramu) 
        //{
          //  $res['status'] = "Success";
          //  $res['pesan']= "Data telah berhasil diubah";
           // return response($res,200);
        //}
        //else if($reservasi && $pembayaran) 
        //{
           // $res['status'] = "Success";
           // $res['pesan']= "Data telah berhasil diubah";
           // return response($res,200);
        //}
        //else if($reservasi) 
        //{
            //$res['status'] = "Success";
            //$res['pesan']= "Data telah berhasil diubah";
            //return response($res,200);
        //}
        //else
        //{
            //$res['status'] = "Success";
           // $res['pesan']= "Tidak ada data yang diubah";
            //return response($res,200);
        //}

    }
    // fungsi menambahkan kd pramuwisata dan membuat rencana perjalanan
    public function create(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_reservasi'      => 'required',
                'Kd_pemandu'        => 'required'
            ]
            );
            $reservasi = Reservasi::where('KD_RESERVASI',$request->Kd_reservasi)->update(['KD_PEMANDUWISATA'=>$request->Kd_pemandu]);
            // $rencanajson=json_decode($request, true);
            // foreach ($request->Rencana as $obj) {
            //     $rencanajson = RencanaPerjalanan::create(
            //         [
            //             'KD_RESERVASI'                    =>$request->Kd_reservasi,
            //             'KETERANGAN'                      =>substr($obj['keterangan'], 0, 10),
            //             'HARIKE_'                         =>substr($obj['harike-'], 0, 1),
            //         ]);
            // }
            // if ($reservasi && $rencanajson) 
            // {
            //     $res['status'] = "Success";
            //     $res['pesan']= "Data telah berhasil diubah";
            //     return response($res,200);
            // }
            if($reservasi) 
            {
                $res['status'] = "Success";
                $res['pesan']= "Data telah berhasil diubah";
                return response($res,200);
            }
            else
            {
                $res['status'] = "Success";
                $res['pesan']= "Tidak ada data yang diubah";
                return response($res,200);
            }
    }
    // fungsi mengambil semua data perjalanan
    public function listobyek($startTime,$endTime,$des)
    {
        $reservasi = Reservasi::Join('tb_jadwal', 'tb_reservasi.TGL_PERJALANAN', '=','tb_jadwal.TGL_MULAI')
        // ->where('tb_reservasi.TGL_PERJALANAN', '=',$startTime)
        ->where('tb_jadwal.TGL_MULAI', '<=' ,$startTime ,'AND', 'tb_jadwal.TGL_MULAI' ,'>=' ,$startTime)
        ->Where('tb_jadwal.TGL_MULAI', '>=' ,$startTime ,'AND', 'tb_jadwal.TGL_BERAKHIR' ,'<=' ,$endTime)
        // ->orWhere('tb_jadwal.TGL_BERAKHIR', '>=' ,$endTime ,'AND', 'tb_jadwal.TGL_BERAKHIR' ,'<=' ,$endTime)
        ->select('tb_reservasi.KD_PEMANDUWISATA');
        $pemandu = PemanduWisata::Join('tb_akun', 'tb_pemanduwisata.KD_AKUN', '=','tb_akun.KD_AKUN')
        ->Join('tb_sertifikat', 'tb_akun.KD_AKUN', '=','tb_sertifikat.KD_AKUN')
        ->select('tb_pemanduwisata.KD_PEMANDUWISATA','tb_akun.KD_AKUN')->where('tb_sertifikat.DOMESTIK', $des)->WhereNotIn('KD_PEMANDUWISATA',$reservasi)->get();
        return($pemandu);
    }
    public function allperjalanan(Request $request)
    {
        $this->validate(
            $request,[
                'Destinasi'         => 'required',
                'Tgl_mulai'         => 'required',
                'Tgl_berakhir'      => 'required',
            ]
            );
            
        $startTime=$request->Tgl_mulai;
        $endTime=$request->tgl_akhir;
        $des=$request->Destinasi;
        $result = $this->listobyek($startTime,$endTime,$des);
        $result1 = array();
        foreach($result as $row)
        {
            $row['Profil']= PemanduWisata::where('KD_PEMANDUWISATA',$row['KD_PEMANDUWISATA'])->select('NAMA_PEMANDUWISATA','PENDIDIKAN')->get();
            $row['Sertifikat']= Sertifikat::where('KD_AKUN',$row['KD_AKUN'])->count();
            $rating=ReviewPemandu::Join('tb_reservasi', 'tb_reviewpemanduwisata.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
            ->where('tb_reservasi.KD_PEMANDUWISATA',$row['KD_PEMANDUWISATA'])
            ->groupBy('tb_reservasi.KD_PEMANDUWISATA')
            ->avg('RATING');
            $row['Rating']=number_format((float)$rating, 2, '.', '');
            $row['Perjalanan yang ditolak']= Riwayat::where('KD_PEMANDUWISATA',$row['KD_PEMANDUWISATA'])->where('STATUS','TOLAK')->count();
            $row['Perjalanan yang diterima']= Riwayat::Join('tb_reservasi', 'tb_riwayatpemandu.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
            ->where('tb_riwayatpemandu.STATUS','5')
            ->where('tb_riwayatpemandu.KD_PEMANDUWISATA',$row['KD_PEMANDUWISATA'])
            ->where('tb_riwayatpemandu.STATUS','TERIMA')->count();
            array_push($result1,$row);
        }
        if($result){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $result;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi untuk membuat reservasi
    
    public function store(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_wisatawan'          => 'required',
                'Kd_pemanduwisata'      => 'required',
                'Kd_paketwisata'        => 'required',
                'Tgl'                   => 'required',
                'Total_pax'             => 'required',
                'Status_perjalanan'     => 'required',

            ]
            );
        $paket = PaketWisata::Join('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
        ->where('tb_paketwisata.KD_PAKETWISATA',$request->Kd_paketwisata)
        ->select('tb_paketwisata.TOTAL', 'tb_destinasi.HARGA_PRAMUWISATA')->first();
        $totalpaket =($request->Total_pax*$paket->TOTAL);
        $jadwal =Jadwal::Join('tb_paketwisata', 'tb_jadwal.KD_PAKETWISATA', '=','tb_paketwisata.KD_PAKETWISATA')
        ->where('tb_paketwisata.KD_PAKETWISATA',$request->Kd_paketwisata)
        ->where('tb_jadwal.TGL_MULAI',$request->Tgl)->pluck('tb_jadwal.TGL_BERAKHIR')->first();
        $datetime1 = Carbon::Parse($request->Tgl);
        $datetime2 = Carbon::Parse($jadwal);
        $interval = $datetime1->diffInDays($datetime2);
        $totalpramu =($interval*$paket->HARGA_PRAMUWISATA);
        $reserid = "U-".Carbon::now()->format('Ymdhhssii');
        $reservasi = Reservasi::create(
        [
            'KD_WISATAWAN'            => $request->Kd_wisatawan,
            'KD_PEMANDUWISATA'        => $request->Kd_pemanduwisata,
            'KD_PAKETWISATA'          => $request->Kd_paketwisata,
            'TGL_PERJALANAN'          => $request->Tgl,
            'TOTAL_PAX'               => $request->Total_pax,
            'TOTAL_PAKET'             => $totalpaket,
            'TOTAL_PRAMUWISATA'       => $totalpramu,
            'TOTAL_TAGIHAN'           => ($totalpaket+$totalpramu),
            'TGL_RESERVASI'           => Carbon::now()->format('Y-m-d'),
            'TGL_UPDATE'              => Carbon::now()->format('Y-m-d'),
            'STATUS_RESERVASI'        => '1',
            'DURASI'                  => $interval,
            'RESERVASI_ID'            => $reserid,
        ]
        );
        
        if($reservasi)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi mengambil semua pembayaran
    public function indexpembayaran(Request $request)
    {
    	$this->validate(
            $request,[
                'status'      		=> 'required',

            ]
            );
        $pembayaran = Pembayaran::join('tb_reservasi','tb_reservasi.KD_RESERVASI','tb_tagihan.KD_RESERVASI')
        ->where('tb_tagihan.STATUS_TAGIHAN',$request->status)
        ->select('*')->get();
    	if(count($pembayaran ) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $pembayaran ;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi mengambil semua pembayaran
    public function indexpembayarankurang(Request $request)
    {
    	$this->validate(
            $request,[
                'status'      		=> 'required',

            ]
            );
        $pembayaran = Pembayaran::join('tb_pengembalian','tb_pengembalian.KD_RESERVASI','tb_tagihan.KD_RESERVASI')
        ->where('tb_tagihan.STATUS_TAGIHAN',$request->status)
        ->select('*')->get();
    	if(count($pembayaran ) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $pembayaran ;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi mengambil semua pembayaran
    public function indexpembayaranlebih(Request $request)
    {
    	$this->validate(
            $request,[
                'status'      		=> 'required',

            ]
            );
        $pembayaran = Pembayaran::join('tb_pengembalian','tb_pengembalian.KD_RESERVASI','tb_tagihan.KD_RESERVASI')
        ->where('tb_tagihan.STATUS_TAGIHAN',$request->status)
        ->select('*')->get();
    	if(count($pembayaran ) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $pembayaran ;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi mengubah status pembayaran
    public function ubahpembayaran(Request $request)
    {

    	$this->validate(
            $request,[
                'kd_tagihan'      => 'required',
                'status'      		=> 'required',
                'nominal'      		=> 'required',
                'kd_akun'      		=> 'required',
                'kd_reservasi'      => 'required',

            ]
            );
        $admin=Admin::where('KD_AKUN', $request->kd_akun)->first();
        $tagihan=Reservasi::where('KD_RESERVASI',$request->kd_reservasi)->pluck('TOTAL_TAGIHAN')->first();
        $selisih=($request->nominal)-$tagihan;
        // echo $selisih;
        if($request->status == 5)
        {
            $pembayaran = Pembayaran::where('KD_TAGIHAN',$request->kd_tagihan)->update(['STATUS_TAGIHAN'=>'2','KD_ADMIN'=>$admin->KD_ADMIN]);
            $reservasi =Reservasi::where('KD_RESERVASI',$request->kd_reservasi)->update(['STATUS_RESERVASI'=>'6']);
            $pengembalian =Pengembalian::create(
                [
                    'KD_RESERVASI' => $request->kd_reservasi,
                    'JUMLAH_TANGGUNGAN' => $selisih*-1,
                    'STATUS' => '1',
                ]
            );
            $tagihan =Pembayaran::create(
                [
                    'KD_RESERVASI' => $request->kd_reservasi,
                    'KD_ADMIN' => $admin,
                    'STATUS_TAGIHAN' => '1',
                ]
            );
            if($tipewisata && $tipewisata && $pengembalian && $tagihan){ //mengecek apakah data kosong atau tidak
                    $res['status'] = "Success";
                    $res['data'] = "Data berhasil diubah";
                    return response($res,201);
                }
            // $res['status'] = "Success";
            // $res['data'] = "Data tidak berubah";
            // $res['data0'] = $selisih;
            // $res['data1'] = "jumlah tagihan pas";
            // return response($res,200);
        }
        else if($request->status == 7)
        {
            $pembayaran = Pembayaran::where('KD_TAGIHAN',$request->kd_tagihan)->update(['STATUS_TAGIHAN'=>'2','KD_ADMIN'=>$admin->KD_ADMIN]);
            $reservasi =Reservasi::where('KD_RESERVASI',$request->kd_reservasi)->update(['STATUS_RESERVASI'=>'7']);
           
            if($tipewisata && $tipewisata){ //mengecek apakah data kosong atau tidak
                    $res['status'] = "Success";
                    $res['data'] = "Data berhasil diubah";
                    return response($res,201);
                }
            // $res['status'] = "Success";
            // $res['data'] = "Data tidak berubah";
            // $res['data0'] = $selisih;
            // $res['data1'] = "jumlah tagihan kurang";
            // return response($res,200);
        }
        else if($request->status == 6)
        {
            $pembayaran = Pembayaran::where('KD_TAGIHAN',$request->kd_tagihan)->update(['STATUS_TAGIHAN'=>'2','KD_ADMIN'=>$admin->KD_ADMIN]);
            $reservasi =Reservasi::where('KD_RESERVASI',$request->kd_reservasi)->update(['STATUS_RESERVASI'=>'6']);
            $pengembalian =Pengembalian::create(
                [
                    'KD_RESERVASI' => $request->kd_reservasi,
                    'JUMLAH_TANGGUNGAN' => $selisih,
                    'STATUS' => '1',
                ]
            );
            $tagihan =Pembayaran::create(
                [
                    'KD_RESERVASI' => $request->kd_reservasi,
                    'KD_ADMIN' => $admin,
                    'STATUS_TAGIHAN' => '1',
                    'NO_REKENING' => $request->norekening,
                    'ATAS_NAMA' => $request->atasnama,
                    'BANK' => $request->bank,
                ]
            );
            if($tipewisata && $tipewisata && $pengembalian &&$tagihan){ //mengecek apakah data kosong atau tidak
                    $res['status'] = "Success";
                    $res['data'] = "Data berhasil diubah";
                    return response($res,201);
                }
            // $res['status'] = "Success";
            // $res['data'] = "Data tidak berubah";
            // $res['data0'] = $selisih;
            // $res['data1'] = "jumlah tagihan lebih";
            // return response($res,200);
        }
    	//$tipewisata = Pembayaran::where('KD_TAGIHAN',$request->kd_tagihan)->update(['STATUS_TAGIHAN'=>$request->status,'KD_ADMIN'=>$admin->KD_ADMIN]);
        //$reservasi =Reservasi::where('KD_RESERVASI',$request->kd_reservasi)->update(['STATUS_RESERVASI'=>'6']);
        // if($tipewisata && $tipewisata){ //mengecek apakah data kosong atau tidak
        //     $res['status'] = "Success";
        //     $res['data'] = "Data berhasil diubah";
        //     return response($res,201);
        // }
        // else{
            // $res['status'] = "Success";
            // $res['data'] = "Data tidak berubah";
            // $res['data1'] = $selisih;
            // $res['data2'] = $tagihan;
            // return response($res,200);
        // }
    }
    // fungsi mengubah status pembayaran
    public function tambahriwayat(Request $request)
    {
    	$this->validate(
            $request,[
                'kd_reservasi'          => 'required',
                'kd_pemandu'      	=> 'required',
                'status'      		=> 'required',

            ]
            );
        $pemandu=PemanduWisata::where('KD_AKUN', $request->kd_pemandu)->first();
    	$riwayat=Riwayat::create(
	        [
	            'KD_PEMANDUWISATA'           => $pemandu->KD_PEMANDUWISATA,
	            'KD_RESERVASI'        	 => $request->kd_pemandu,
	            'STATUS'         		 => $request->status,
	        ]
	);
    	if($riwayat){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = "Data berhasil diubah";
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data tidak berubah";
            return response($res,200);
        }
    }
    
}

