<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMailable;
use App\Mail\ContactMailable;
use Mail;
use Carbon\Carbon;
use App\Models\Akun;
use App\Models\Bahasa;
// use App\Token;
use App\Models\Admin;
use App\Models\BiroTravel;
use App\Models\PemanduWisata;
use App\Models\Sertifikat;
use App\Models\Rekening;
use App\Models\TipeAkun;
use App\Models\Riwayat;
use App\Models\Review;
use App\Models\ReviewPemandu;
use App\Models\SaldoBiro;
use App\Models\SaldoPemandu;
use Illuminate\Support\Facades\Input;
use Auth;

class AkunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate(
            $request, [
                'Kd_tipeakun' => 'required',
                'Status'
            ]
        );
        if ($request->Status != null) {
            if($request->Status=='Verifikasi')
            {
                
                if ($request->Kd_tipeakun == 'KTA-0005') {
                    $data1 = Akun::join('tb_birotravel', 'tb_akun.KD_AKUN', 'tb_birotravel.KD_AKUN')
                        // ->leftjoin('tb_sertifikat','tb_akun.KD_AKUN','tb_sertifikat.KD_AKUN')
                        ->select('tb_akun.KD_AKUN','tb_akun.EMAIL','tb_akun.FOTO','tb_birotravel.NAMA_BIRO','tb_birotravel.NAMA_BIRO','tb_birotravel.NOTELP_BIRO','tb_birotravel.NAMAPJ_BIRO','tb_birotravel.NIKPJ_BIRO','tb_birotravel.JABATANPJ_BIRO')
                        ->where('KD_TIPEAKUN', $request->Kd_tipeakun)->where('STATUS', $request->Status)->get();
                } else if ($request->Kd_tipeakun == 'KTA-0004') {
                    $data1 = Akun::join('tb_pemanduwisata', 'tb_akun.KD_AKUN', 'tb_pemanduwisata.KD_AKUN')
                        // ->leftjoin('tb_sertifikat','tb_akun.KD_AKUN','tb_sertifikat.KD_AKUN')
                        ->select('tb_akun.KD_AKUN','tb_akun.EMAIL','tb_akun.FOTO','tb_pemanduwisata.NAMA_PEMANDUWISATA','tb_pemanduwisata.NIK_PEMANDUWISATA','tb_pemanduwisata.JK_PEMANDUWISATA','tb_pemanduwisata.NOTELP_PEMANDUWISATA','tb_pemanduwisata.DOMISILI','tb_pemanduwisata.PENDIDIKAN')
                        ->where('KD_TIPEAKUN', $request->Kd_tipeakun)->where('STATUS', $request->Status)->get();
                }
            }
            else
            {
                if ($request->Kd_tipeakun == 'KTA-0005') {
                    $data1 = Akun::join('tb_birotravel', 'tb_akun.KD_AKUN', 'tb_birotravel.KD_AKUN')
                        ->leftjoin('tb_sertifikat','tb_akun.KD_AKUN','tb_sertifikat.KD_AKUN')
                        ->select('tb_akun.KD_AKUN','tb_akun.EMAIL','tb_akun.FOTO','tb_birotravel.NAMA_BIRO','tb_birotravel.NAMA_BIRO','tb_birotravel.NOTELP_BIRO','tb_birotravel.NAMAPJ_BIRO','tb_birotravel.NIKPJ_BIRO','tb_birotravel.JABATANPJ_BIRO','tb_sertifikat.URL_SERTIFIKAT','tb_sertifikat.DOMESTIK')
                        ->where('KD_TIPEAKUN', $request->Kd_tipeakun)->where('STATUS', $request->Status)->get();
                } else if ($request->Kd_tipeakun == 'KTA-0004') {
                    $data1 = Akun::join('tb_pemanduwisata', 'tb_akun.KD_AKUN', 'tb_pemanduwisata.KD_AKUN')
                        ->leftjoin('tb_sertifikat','tb_akun.KD_AKUN','tb_sertifikat.KD_AKUN')
                        ->select('tb_akun.KD_AKUN','tb_akun.EMAIL','tb_akun.FOTO','tb_pemanduwisata.NAMA_PEMANDUWISATA','tb_pemanduwisata.NIK_PEMANDUWISATA','tb_pemanduwisata.JK_PEMANDUWISATA','tb_pemanduwisata.NOTELP_PEMANDUWISATA','tb_pemanduwisata.DOMISILI','tb_pemanduwisata.PENDIDIKAN','tb_sertifikat.URL_SERTIFIKAT','tb_sertifikat.DOMESTIK')
                        ->where('KD_TIPEAKUN', $request->Kd_tipeakun)->where('STATUS', $request->Status)->get();
                }
            }
        } else {
            if ($request->Kd_tipeakun == 'KTA-0002') {
                $data1 = Akun::join('tb_admin', 'tb_akun.KD_AKUN', 'tb_admin.KD_AKUN')->where('KD_TIPEAKUN', $request->Kd_tipeakun)
                    ->select('*')->get();
                //$sertifikat =Sertifikat::where('KD_AKUN', $request->Kd_akun)->get();
            } else {
                $data1 = Akun::where('KD_TIPEAKUN', $request->Kd_tipeakun)->get();
                //$sertifikat =Sertifikat::where('KD_AKUN', $request->Kd_akun)->get();
            }
        }

        if (count($data1) > 0) { //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $data1;
            return response($res);
        } else {
            $res['status'] = "Success";
            $res['data'] = "data tidak ada";
            return response($res);
        }
    }

    // fungsi mengubah status akun
    public function update(Request $request)
    {
        $this->validate(
            $request, [
                'Kd_akun' => 'required',
                'Status' => 'required',
            ]
        );
        $akun = Akun::where('KD_AKUN', $request->Kd_akun)->update(['STATUS' => $request->Status]);
        if ($akun) {
            $akun1 = Akun::where('KD_AKUN', $request->Kd_akun)->pluck('EMAIL')->first();
            $email = array('email' => $akun, 'subject' => 'Verifikasi Akun', 'bodyMessage' => 'Selamat datang di Vaithme, Akun anda telah kami verifikasi.');
            // Mail::send('email.send',$email,function ($message)use($email)
            // {

            //     $message->to($email['email']);
            //     $message->subject($email['subject']);
            //     $message->from($email['Vaithme2018@gmail.com']);
            // });
            $name = 'Selamat datang di Vaithme, Akun anda telah kami verifikasi.';
            Mail::to($akun1)->send(new SendMailable($name));
            $res['status'] = "Success";
            $res['pesan'] = "Data telah berhasil diubah";
            return response($res, 200);
        } else {
            $res['status'] = "Success";
            $res['pesan'] = "Tidak ada data yang diubah";
            return response($res, 200);
        }

    }

    // fungsi registrasi
    public function store(Request $request)
    {
        $this->validate(
            $request, [
                'Email' => 'required|email|unique:tb_akun',
                'Password' => 'required|min:5',
                'Tipe' => 'required'
            ]
        );
        if ($request->Tipe == 'Admin') {
            $akun = Akun::create(
                [
                    'EMAIL' => $request->Email,
                    'USERNAME' => $request->Username,
                    'PASSWORD' => $request->Password,
                    'KD_TIPEAKUN' => 'KTA-0002',
                    'STATUS' => 'Verivikasi',
                    'FOTO' => $request->Foto,
                ]
            );
            $akun->admin = Admin::create(
                [
                    'KD_AKUN' => $akun->id,
                    'NAMA_ADMIN' => $request->Nama,
                    'NIK_ADMIN' => $request->Nik,
                    'TGL_UPDATE' => Carbon::now()
                ]
            );
        } else if ($request->Tipe == 'Biro Travel') {

            $username = "U-" . rand(1, 999999);
            $akun = Akun::create(
                [
                    'EMAIL' => $request->Email,
                    'USERNAME' => $username,
                    'PASSWORD' => $request->Password,
                    'KD_TIPEAKUN' => 'KTA-0005',
                    'STATUS' => 'Belum Terverifikasi',
                    'FOTO' => $request->Foto,
                ]
            );
            $akun->biro = BiroTravel::create(
                [
                    'KD_AKUN' => $akun->id,
                    'NAMA_BIRO' => $request->Nama,
                    'NAMAPJ_BIRO' => $request->NamaPJ,
                    'NIKPJ_BIRO' => $request->Nik,
                    'JABATANPJ_BIRO' => $request->Jabatan,
                    'ALAMAT_BIRO' => $request->Alamat,
                    'NOTELP_BIRO' => $request->Telp,
                    'TERM'  =>'Semua Pembatalan pemesanan diperlukan secara tertulis. Namun, biaya pembatalan akan berlaku dan dihitung berdasarkan jangka waktu pemberitahuan yang diberikan sebelum keberangkatan. Biaya pembatalan serta komponen pengembalian uang yang sesuai ditunjukkan pada kebijakan pembatalan / pengembalian masing-masing produk tur masing-masing.',
                    'TGL_UPDATE' => Carbon::now()

                ]
            );

        } else if ($request->Tipe == 'Pemandu Wisata') {
            $akun = Akun::create(
                [
                    'EMAIL' => $request->Email,
                    'USERNAME' => $request->Username,
                    'PASSWORD' => $request->Password,
                    'KD_TIPEAKUN' => 'KTA-0004',
                    'STATUS' => 'Belum Terverifikasi',
                    'FOTO' => $request->Foto,
                ]
            );
            $akun->pemandu = PemanduWisata::create(
                [
                    'KD_AKUN' => $akun->id,
                    'NAMA_PEMANDUWISATA' => $request->Nama,
                    'NIK_PEMANDUWISATA' => $request->Nik,
                    'PENDIDIKAN' => $request->Pendidikan,
                    'JK_PEMANDUWISATA' => $request->JK,
                    'DOMISILI'  => $request->Domisili,
                    'NOTELP_PEMANDUWISATA'  => $request->Telp,
                    'PENGALAMAN_PEMANDUWISATA'  => $request->Pengalaman,
                    'TGL_UPDATE' => Carbon::now()
                ]
            );
            $akun->bahasa =Bahasa::create(
                [
                    'KD_AKUN' => $akun->id,
                    'BAHASA' => $request->bahasa,
                    'ENGLISH' => $request->english,
                    'LAINNYA' => $request->lainnya,
                ]
            );
        } else {
            $res['status'] = "Success";
            $res['data'] = "Wisatawan ya";
            return response($res, 200);
        }
        if ($akun)
        {
            
            $res['status'] = "Success";
            $res['pesan'] = "Data telah sukses ditambahkan";
            return response($res, 201);
        }
        else
        {
            
            $res['status'] = "Success";
            $res['pesan'] = "Data gagal ditambahkan";
            return response($res, 200);
        }
    }

    // fungsi login sementara

    public function login(Request $request, Akun $akun)
    {
        $this->validate(
            $request, [
                'Email' => 'required|email',
                'Password' => 'required',
            ]
        );

        $akun = Akun::where('EMAIL', $request->Email)->where('PASSWORD', $request->Password)->where('STATUS', 'Verifikasi')->first();
        if ($akun) {
            $status = TipeAkun::where('KD_TIPEAKUN', $akun->KD_TIPEAKUN)->first();
            if ($status->TIPE_AKUN == "Biro Travel") {
                $nama = BiroTravel::where('KD_AKUN', $akun->KD_AKUN)->first();
                $pemasukkan = SaldoBiro::where('KD_BIROTRAVEL', $nama->KD_BIROTRAVEL)->get()->sum('TOTAL');
                if (count($pemasukkan) > 0) {

                    $result = $pemasukkan;
                } else {
                    $result = 0;
                }
                $data['Akun'] = $akun;
                $data1['TipeAkun'] = $status->TIPE_AKUN;
                $data1['Nama'] = $nama->NAMA_BIRO;
                $data1['Pemasukkan'] = $result;
                $data['Keterangan'] = $data1;
                $res['status'] = "Success";
                $res['data'] = $data;
                return response($res, 200);
            } else if ($status->TIPE_AKUN == "Pemandu Wisata") {
                $nama = PemanduWisata::where('KD_AKUN', $akun->KD_AKUN)->first();
                $pemasukkan = SaldoPemandu::where('KD_PEMANDUWISATA', $nama->KD_PEMANDUWISATA)->get()->sum('TOTAL');
                if (count($pemasukkan) > 0) {
                    //echo ($pemasukkan);
                    $result = $pemasukkan;
                } else {
                    $result = 0;
                }
                $data['Akun'] = $akun;
                $data1['TipeAkun'] = $status->TIPE_AKUN;
                $data1['Nama'] = $nama->NAMA_PEMANDUWISATA;
                $data1['Pemasukkan'] = $result;
                $data['Keterangan'] = $data1;
                $res['status'] = "Success";
                $res['data'] = $data;
                return response($res, 200);
            } else {
                $data['Akun'] = $akun;
                $data['TipeAkun'] = $status;
                $res['status'] = "Success";
                $res['data'] = $data;
                return response($res, 200);
            }
        } else {
            $res['status'] = "Success";
            $res['data'] = "Pastikan Email dan Password yang anda masukkan benar";
            return response($res, 200);
        }
    }

//fungsi cek username
// fungsi gak dipake lagi 
    public function cekusername(Request $request)
    {
        $this->validate(
            $request, [
                'Username' => 'required',
            ]
        );

        $username = Akun::where('USERNAME', $request->Username)->first();
        if ($username) {
            $data['Email'] = null;
            $data['Username'] = "the username has already been taken";
            $res['status'] = "Success";
            $res['message'] = "The given data has was invalid";
            $res['error'] = $data;
            return response($res, 200);
        } else {
            $res['status'] = "Success";
            $res['message'] = "available";
            return response($res, 200);
        }
    }

    //fungsi cek email
    public function cekemail(Request $request)
    {
        $this->validate(
            $request, [
                'Email' => 'required|email',
            ]
        );

        $akun = Akun::where('EMAIL', $request->Email)->first();
        if ($akun) {
            $data['Email'] = "the email has already been taken";
            $res['status'] = "Success";
            $res['message'] = "The given data has was invalid";
            $res['error'] = $data;
            return response($res, 200);
        } else {
            $res['status'] = "Success";
            $res['message'] = "available";
            return response($res, 200);
        }
    }

    // Fungsi update profil
    public function ubahprofil(Request $request)
    {
        $this->validate(
            $request, [
                'Kd_akun' => 'required',
            ]
        );
        $akun = Akun::where('KD_AKUN', $request->Kd_akun)->first();
        //ADMIN
        if ($akun->KD_TIPEAKUN == 'KTA-0002') {
            $this->validate(
                $request, [
                    'Password' => 'required',
                ]
            );
            $akun = Akun::where('KD_AKUN', $request->Kd_akun)->update(['PASSWORD' => $request->Password, 'TGL_UPDATE' => Carbon::now()]);
            if ($akun) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 201);
            } else {
                $res['status'] = "Success";
                $res['pesan'] = "tidak ada data yang diubah";
                return response($res, 200);
            }
        } //BIRO TRAVEL
        else if ($akun->KD_TIPEAKUN == 'KTA-0005') {
            $this->validate(
                $request, [
                    'Email' => 'required|email',
                    'Nama' => 'required',
                    'NamaPJ' => 'required',
                    'Nik' => 'required',
                    'Jabatan' => 'required',
                    'Notelp' => 'required',
                    'Alamat' => 'required',
                    'EmailPJ_Biro' => 'required|email',
                ]
            );
            $akun = Akun::where('KD_AKUN', $request->Kd_akun)->update(['EMAIL' => $request->Email]);
            $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->update(['NAMA_BIRO' => $request->Nama, 'NAMAPJ_BIRO' => $request->NamaPJ, 'NIKPJ_BIRO' => $request->Nik, 'JABATANPJ_BIRO' => $request->Jabatan, 'NOTELP_BIRO' => $request->Notelp, 'ALAMAT_BIRO' => $request->Alamat, 'EMAILPJ_BIRO' => $request->EmailPJ_Biro, 'TGL_UPDATE' => Carbon::now()]);
            if ($akun && $biro) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            } else if ($akun) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            } else if ($biro) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            } else {
                $res['status'] = "Success";
                $res['pesan'] = "tidak ada data yang diubah";
                return response($res, 200);
            }
        } // PRAMUWISATA
        else if ($akun->KD_TIPEAKUN == 'KTA-0004') {
            $this->validate(
                $request, [
                    'Foto' => 'required',
                    'Email' => 'required',
                    'JenisKelamin' => 'required',
                    'TL' => 'required',
                    'NoTelp' => 'required',
                    'Pendidikan' => 'required',
                    'Pengalaman' => 'required',
                    'Domisili'=> 'required',

                ]
            );
            // $date = date_format($request->TL,'DD-MM-YYYY');
            $akun = Akun::where('KD_AKUN', $request->Kd_akun)->update(['EMAIL' => $request->Email, 'TGL_UPDATE' => Carbon::now()]);
            $pemandu = PemanduWisata::where('KD_AKUN', $request->Kd_akun)->update(['JK_PEMANDUWISATA' => $request->JenisKelamin, 'TL_PEMANDUWISATA' => $request->TL, 'NOTELP_PEMANDUWISATA' => $request->NoTelp, 'TGL_UPDATE' => Carbon::now(), 'PENDIDIKAN' => $request->Pendidikan, 'DOMISILI' => $request->Domisili, 'PENGALAMAN _PEMANDUWISATA' => $request->Pengalaman]);
            $bahasa = BAHASA::where('KD_AKUN', $request->Kd_akun)->update(['BAHASA' => $request->Bahasa, 'ENGLISH' => $request->English, 'LAINNYA' => $request->Lainnya]);
            if ($akun && $pemandu && $bahasa) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            } 
            else if ($akun && $pemandu) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            }
            else if ($akun && $bahasa) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            }else if ($pemandu && $bahasa) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            }else if ($akun) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            } else if ($pemandu) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            } else if ($bahasa) {
                $res['status'] = "Success";
                $res['pesan'] = "Data telah berhasil diubah";
                return response($res, 200);
            }else {
                $res['status'] = "Success";
                $res['pesan'] = "tidak ada data yang diubah";
                return response($res, 200);
            }
        } else {
            $res['status'] = "Success";
            $res['pesan'] = "Apakah Anda Wisatawan ?";
            return response($res, 200);
        }
    }

    // Fungsi menampilkan profil
    public function getprofil(Request $request)
    {
        $this->validate(
            $request, [
                'Kd_akun' => 'required',
            ]
        );
        $akun = Akun::where('KD_AKUN', $request->Kd_akun)->first();
        //ADMIN
        if ($akun->KD_TIPEAKUN == 'KTA-0002') {
            $profil = Akun::Join('tb_admin', 'tb_admin.KD_AKUN', '=', 'tb_akun.KD_AKUN')
                ->select('tb_akun.KD_AKUN', 'tb_akun.USERNAME', 'tb_akun.EMAIL', 'tb_akun.FOTO', 'tb_admin.NAMA_ADMIN', 'tb_admin.NIK_ADMIN')
                ->where('tb_akun.KD_AKUN', $request->Kd_akun)
                ->first();
            $res['status'] = "Success";
            $res['data'] = $profil;
            return response($res, 200);
        } //BIRO TRAVEL
        else if ($akun->KD_TIPEAKUN == 'KTA-0005') {
            $profil = Akun::Join('tb_birotravel', 'tb_birotravel.KD_AKUN', '=', 'tb_akun.KD_AKUN')
                ->select('tb_akun.KD_AKUN', 'tb_akun.EMAIL', 'tb_akun.FOTO', 'tb_birotravel.NAMA_BIRO', 'tb_birotravel.ALAMAT_BIRO',
                    'tb_birotravel.NOTELP_BIRO', 'tb_birotravel.NAMAPJ_BIRO', 'tb_birotravel.NIKPJ_BIRO', 'tb_birotravel.EMAILPJ_BIRO', 'tb_birotravel.JABATANPJ_BIRO', 'tb_birotravel.TERM','tb_birotravel.KD_BIROTRAVEL')
                ->where('tb_akun.KD_AKUN', $request->Kd_akun)
                ->first();
            $sertifikat = Sertifikat::where('KD_AKUN', $request->Kd_akun)->get();
            $rekening = Rekening::where('KD_AKUN', $request->Kd_akun)->get();
            $rating = Review::Join('tb_reservasi', 'tb_review.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
            ->Join('tb_paketwisata', 'tb_paketwisata.KD_PAKETWISATA', '=','tb_reservasi.KD_PAKETWISATA')
            ->select('tb_review.RATING')
            ->where('tb_paketwisata.KD_BIROTRAVEL', $profil->KD_BIROTRAVEL)
            ->avg('RATING');
            $data['Rating'] = $rating;
            $data['Profil'] = $profil;
            $data['Sertifikat'] = $sertifikat;
            $data['Rekening'] = $rekening;
            $data['Keterangan'] = 'test';
            $res['status'] = "Success";
            $res['data'] = $data;
            return response($res, 200);
        } // PRAMUWISATA
        else if ($akun->KD_TIPEAKUN == 'KTA-0004') {
            $profil = Akun::Join('tb_pemanduwisata', 'tb_pemanduwisata.KD_AKUN', '=', 'tb_akun.KD_AKUN')
                ->select('tb_akun.KD_AKUN', 'tb_akun.EMAIL', 'tb_akun.FOTO', 'tb_pemanduwisata.KD_PEMANDUWISATA', 'tb_pemanduwisata.NAMA_PEMANDUWISATA', 'tb_pemanduwisata.NIK_PEMANDUWISATA',
                    'tb_pemanduwisata.JK_PEMANDUWISATA', 'tb_pemanduwisata.TL_PEMANDUWISATA', 'tb_pemanduwisata.NOTELP_PEMANDUWISATA', 'tb_pemanduwisata.PENDIDIKAN','tb_pemanduwisata.KD_PEMANDUWISATA')
                ->where('tb_akun.KD_AKUN', $request->Kd_akun)
                ->first();
            $totalpaket = Riwayat:: where('KD_PEMANDUWISATA', $profil->KD_PEMANDUWISATA)->where('STATUS', 'Selesai')->count();
            $sertifikat = Sertifikat::where('KD_AKUN', $request->Kd_akun)->get();
            $rekening = Rekening::where('KD_AKUN', $request->Kd_akun)->get();
            $rating = ReviewPemandu::Join('tb_reservasi', 'tb_reviewpemanduwisata.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
            ->select('tb_reviewpemanduwisata.RATING')
            ->where('tb_reservasi.KD_PEMANDUWISATA', $profil->KD_PEMANDUWISATA)
            ->avg('tb_reviewpemanduwisata.RATING');
            $bahasa = Bahasa::where('KD_AKUN', $request->Kd_akun)->first();
            $data['Bahasa'] =$bahasa;
            $data['Rating'] = $rating;
            $data['Profil'] = $profil;
            $data['Sertifikat'] = $sertifikat;
            $data['Rekening'] = $rekening;
            $lain['Total Paket'] = $totalpaket;
            $lain['Total biro'] = $totalpaket;
            $data['Keterangan'] = $lain;
            $res['status'] = "Success";
            $res['data'] = $data;
            return response($res, 200);
        } else {
            $res['status'] = "Success";
            $res['data'] = $profil;
            return response($res, 200);
        }
    }

    // menghitung jumlah akun yang ada
    public function countaccount()
    {
        $admin = Akun::where('KD_TIPEAKUN', 'KTA-0002')->count();
        $wisatawan = Akun::where('KD_TIPEAKUN', 'KTA-0003')->count();
        $birotravel = Akun::where('KD_TIPEAKUN', 'KTA-0005')->where('STATUS', 'Verifikasi')->count();
        $pramuwisata = Akun::where('KD_TIPEAKUN', 'KTA-0004')->where('STATUS', 'Verifikasi')->count();

        $data['admin'] = $admin;
        $data['wisatawan'] = $wisatawan;
        $data['birotravel'] = $birotravel;
        $data['pramuwisata'] = $pramuwisata;
        $res['status'] = "Success";
        $res['data'] = $data;
        return response($res);

    }

    // fungsi untuk mengirimkan email

    public function email(Request $req)
    {
        $this->validate(
            $req, [
                'email' => 'required|email',
                'pesan' => 'required',
            ]
        );
        // $request=$data;
        $jadwal = json_decode($req, true);
        $json = json_decode($req->email, true);
        $json1 = json_decode($req->pesan, true);
        $sendemail = Mail::to('zkhputri57@gmail.com')->send(new ContactMailable($json, $json1));
        if ($sendemail) {

            $res['status'] = "Success";
            $res['pesan'] = "Data berhasil dikirim";
            return response($res, 200);
        } else {
            $res['status'] = "Success";
            $res['pesan'] = "Data gagal dikirim";
            return response($req, 200);
        }
    }
    public function uploadsertifikat(Request $request)
    {
        $rules = array(
            'domestik' => 'required',
            'kd_akun'=>'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
            // |max:2048'
        );
        $image = $request->file("image");
            $imgname = time() . '.' . $image->getClientOriginalExtension();;
            $destinationPath = public_path('images/sertifikat/');
            $image->move($destinationPath, $imgname);
            // SAVE TO DB
            $sertifikat = Sertifikat::create([
                'KD_AKUN' => Input::get("kd_akun"),
                'URL_SERTIFIKAT' => image_path('sertifikat') . $imgname,
                'DOMESTIK' => Input::get("domestik"),
            ]);
        if ($image) {

            $res['status'] = "Success";
            $res['pesan'] = "Data berhasil dikirim";
            return response($res, 201);
        } else {
            $res['status'] = "Success";
            $res['pesan'] = "Data gagal dikirim";
            return response($req, 200);
            }
    }
    // fungsi registrasi dengan formdata
    // public function storebaru(Request $request)
    // {
    //     $rules = array(
    //         'Email' => 'required|email|unique:tb_akun',
    //         'Password' => 'required|min:5',
    //         'Tipe' => 'required',
    //         'domestik' => 'required',
    //         'kd_akun'=>'required',
    //         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
    //         'sertifikat' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
    //         // |max:2048'
    //     );
    //     $image = $request->file("image");
    //         $imgname = time() . '.' . $image->getClientOriginalExtension();;
    //         $destinationPath = public_path('images/sertifikat/');
    //         $image->move($destinationPath, $imgname);
    //     $sertifikat = $request->file("sertifikat");
    //         $sername = time() . '.' . $image->getClientOriginalExtension();;
    //         $destinationPath = public_path('images/sertifikat/');
    //         $sertifikat->move($destinationPath, $sername);
    //     if ($request->Tipe == 'Admin') {
    //         $akun = Akun::create(
    //             [
    //                 'EMAIL' => Input::get("Email"),
    //                 'USERNAME' => Input::get("Username"),
    //                 'PASSWORD' => Input::get("Password"),
    //                 'KD_TIPEAKUN' => 'KTA-0002',
    //                 'STATUS' => 'Verivikasi',
    //                 'FOTO' => image_path('image') . $imgname,
    //             ]
    //         );
    //         $akun->admin = Admin::create(
    //             [
    //                 'KD_AKUN' => $akun->id,
    //                 'NAMA_ADMIN' =>Input::get("Nama"),
    //                 'NIK_ADMIN' => Input::get("Nik"),
    //                 'TGL_UPDATE' => Carbon::now()
    //             ]
    //         );
    //     } else if ($request->Tipe == 'Biro Travel') {

    //         $username = "U-" . rand(1, 999999);
    //         $akun = Akun::create(
    //             [
    //                 'EMAIL' =>Input::get("Email"),
    //                 'USERNAME' => $username,
    //                 'PASSWORD' => Input::get("Password"),
    //                 'KD_TIPEAKUN' => 'KTA-0005',
    //                 'STATUS' => 'Belum Terverivikasi',
    //                 'FOTO' => image_path('sertifikat') . $imgname,
    //             ]
    //         );
    //         $akun->biro = BiroTravel::create(
    //             [
    //                 'KD_AKUN' => $akun->id,
    //                 'NAMA_BIRO' => Input::get("Nama"),
    //                 'NAMAPJ_BIRO' => Input::get("NamaPJ"),
    //                 'NIKPJ_BIRO' => Input::get("Nik"),
    //                 'JABATANPJ_BIRO' => Input::get("Jabatan"),
    //                 'ALAMAT_BIRO' => Input::get("Alamat"),
    //                 'TERM'  =>'Semua Pembatalan pemesanan diperlukan secara tertulis. Namun, biaya pembatalan akan berlaku dan dihitung berdasarkan jangka waktu pemberitahuan yang diberikan sebelum keberangkatan. Biaya pembatalan serta komponen pengembalian uang yang sesuai ditunjukkan pada kebijakan pembatalan / pengembalian masing-masing produk tur masing-masing.',
    //                 'TGL_UPDATE' => Carbon::now()

    //             ]
    //         );
    //         $akun->sertifikat = Sertifikat::create(
    //             [
    //                 'KD_AKUN' => Input::get("kd_akun"),
    //                 'URL_SERTIFIKAT' => image_path('sertifikat') . $sername,
    //                 'DOMESTIK' => Input::get("domestik"),
    //             ]
    //             );
    //     } else if ($request->Tipe == 'Pemandu Wisata') {
    //         $akun = Akun::create(
    //             [
    //                 'EMAIL' => Input::get("Email"),
    //                 'USERNAME' => Input::get("Username"),
    //                 'PASSWORD' => Input::get("Password"),
    //                 'KD_TIPEAKUN' => 'KTA-0004',
    //                 'STATUS' => 'Belum Terverifikasi',
    //                 'FOTO' => image_path('image') . $imgname,
    //             ]
    //         );
    //         $akun->pemandu = PemanduWisata::create(
    //             [
    //                 'KD_AKUN' => $akun->id,
    //                 'NAMA_PEMANDUWISATA' => Input::get("Nama"),
    //                 'NIK_PEMANDUWISATA' => Input::get("Nik"),
    //                 'PENDIDIKAN' => Input::get("Pendidikan"),
    //                 'JK_PEMANDUWISATA' => Input::get("JK"),
    //                 'TGL_UPDATE' => Carbon::now(),
    //                 'DOMISILI' => Input::get("Domisili"),
    //             ]
    //         );
    //         $akun->bahasa =Bahasa::create(
    //             [
    //                 'KD_AKUN' => $akun->id,
    //                 'BAHASA' => Input::get("Bahasa"),
    //                 'ENGLISH' => Input::get("English"),
    //                 'LAINNYA' => Input::get("Lainnya"),
    //             ]
    //         );
    //         $akun->sertifikat = Sertifikat::create(
    //             [
    //                 'KD_AKUN' => $akun->id,
    //                 'URL_SERTIFIKAT' => image_path('sertifikat') . $sername,
    //                 'DOMESTIK' => Input::get("domestik"),
    //             ]
    //             );
    //     } else {
    //         $res['status'] = "Success";
    //         $res['data'] = "Wisatawan ya";
    //         return response($res, 200);
    //     }
    //     if ($akun)
    //     {
            
    //         $res['status'] = "Success";
    //         $res['pesan'] = "Data telah sukses ditambahkan";
    //         return response($res, 201);
    //     }
    //     else
    //     {
            
    //         $res['status'] = "Success";
    //         $res['pesan'] = "Data gagal ditambahkan";
    //         return response($res, 200);
    //     }
    // }
}
