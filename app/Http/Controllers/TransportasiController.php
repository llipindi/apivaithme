<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transportasi;
use App\Models\JnsTransportasi;
use App\Models\BiroTravel;
use App\Models\TransportasiDet;

class TransportasiController extends Controller
{
    // fungsi menambahkan jenis transportasi admin
    public function storejnstransportasi(Request $request)
    {
        $this->validate(
            $request,[
                'Jnstransportasi'              => 'required',
                'Deskripsi'                    => 'required',
            ]
            );
            $jnstransportasi = JnsTransportasi::create(
            [
                'NAMA_JENISTRANSPORTASI'       =>$request->Jnstransportasi,
                'DESKRIPSI_JENISTRANSPORTASI'  =>$request->Deskripsi,
            ]
            );
        if($jnstransportasi)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi mengubah jenis transportasi admin
    public function updatejnstransportasi(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_jnstransportasi'      => 'required',
                'Jnstransportasi'         => 'required',
                'Deskripsi'               => 'required',

            ]
            );
        $jnstransportasi = JnsTransportasi::where('KD_JENISTRANSPORTASI',$request->Kd_jnstransportasi)->update(['NAMA_JENISTRANSPORTASI'=>$request->Jnstransportasi, 'DESKRIPSI_JENISTRANSPORTASI'=>$request->Deskripsi]);
        if ($jnstransportasi) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
    // fungsi menampilkan jenis transportasi
    public function indexjnstransportasi()
    {
        $jnstransportasi = JnsTransportasi::all();

        if(count($jnstransportasi) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $jnstransportasi;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi menambahkan transportasi biro travel
    public function storetransportasi(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_jnstransportasi'            => 'required',
                'Kd_akun'                       => 'required',
                'Nama'                          => 'required',
                'Harga'                         => 'required',
                'Foto'                          => 'required',
            ]
            );
            $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
            $transportasi = Transportasi::create(
            [
                'KD_JENISTRANSPORTASI'       =>$request->Kd_jnstransportasi,
                'KD_BIROTRAVEL'              =>$biro->KD_BIROTRAVEL,
                'NAMA_TRANSPORTASI'          =>$request->Nama,
                'HARGA'                      =>$request->Harga,
                'FOTO_TRANSPORTASI'          =>$request->Foto,
            ]
            );
        if($transportasi)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi menampilkan transportasi biro travel
    public function transportasi($id, $trans)
    {
        $transportasi = Transportasi::Join('tb_jnstransportasi', 'tb_jnstransportasi.KD_JENISTRANSPORTASI', '=','tb_transportasi.KD_JENISTRANSPORTASI')
        ->select('tb_transportasi.KD_TRANSPORTASI', 'tb_jnstransportasi.NAMA_JENISTRANSPORTASI', 'tb_transportasi.NAMA_TRANSPORTASI', 'tb_transportasi.HARGA', 'tb_transportasi.FOTO_TRANSPORTASI')
        ->where('tb_transportasi.KD_BIROTRAVEL', $id)
        ->where('tb_transportasi.KD_JENISTRANSPORTASI', $trans)
        ->get();
        return $transportasi;
    }
    public function indextransportasi(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'      => 'required',
            ]
            );
        $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
        $transportasi = JnsTransportasi::all();
        $result = array();
        foreach($transportasi as $row)
        {
            $row['Transportasi']=$this->transportasi($biro->KD_BIROTRAVEL,$row['KD_JENISTRANSPORTASI']);
            array_push($result,$row);
        }
        if(count($result) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $result;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi mengubah transportasi biro travel
    public function updatetransportasi(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_transportasi'                 => 'required',
                'Kd_jnstransportasi'              => 'required',
                'Nama'                            => 'required',
                'Harga'                           => 'required',
                'Foto'                            => 'required',
            ]
            );
        $transportasi = Transportasi::where('KD_TRANSPORTASI',$request->Kd_transportasi)
        ->update(['NAMA_TRANSPORTASI'=>$request->Nama, 'KD_JENISTRANSPORTASI'=>$request->Kd_jnstransportasi, 'HARGA'=>$request->Harga, 'FOTO_TRANSPORTASI'=>$request->Foto]);
        if ($transportasi) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
    
    // menghapus transportasi detail biro travel
    public function delete(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_transportasi'                 => 'required',
            ]
            );
        $cari = TransportasiDet::where('KD_TRANSPORTASI',$request->Kd_transportasi)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $transportasi = Transportasi::where('KD_TRANSPORTASI',$request->Kd_transportasi)->delete();
            // $penginapan->delete();
            if($transportasi)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,200);
            }
        }
    }
    // menghapus transportasi
    public function deletejns(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_jnstransportasi'                 => 'required',
            ]
            );
        $cari = Transportasi::where('KD_JENISTRANSPORTASI',$request->Kd_jnstransportasi)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $transportasi = JnsTransportasi::where('KD_JENISTRANSPORTASI',$request->Kd_jnstransportasi)->delete();
            if($transportasi)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,204);
            }
        }
    }
    // fungsi menambahkan transportasi detail untuk paket wisata
    // public function storetransportasidet(Request $request)
    // {
    //     $transportasi=TransportasiDet::insert($request[0]);

    //     if($transportasi )
    //     { 
    //         $res['status'] = "Success";
    //         $res['data'] = "Data berhasil ditambahkan";
    //         return response($res);
    //     }
    //     else{
    //         $res['status'] = "Success";
    //         $res['data'] = "Data gagal ditambahkan";
    //         return response($res,200);
    //     }
    // }
}
