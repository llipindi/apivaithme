<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inclusion;
use App\Models\InclusionDet;

class InclusionController extends Controller
{
    // fungsi menambahkan inclusion admin
    public function store(Request $request)
    {
        $this->validate(
            $request,[
                'Inclusion'             => 'required',
            ]
            );
            $inclusion = Inclusion::create(
            [
                'KETERANGAN'        => $request->Inclusion,
            ]
            );
        if($inclusion)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi mengubah inclusion admin
    public function update(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_inclusion'      => 'required',
                'Inclusion'         => 'required',
            ]
            );
        $inclusion = Inclusion::where('KD_INCLUSION',$request->Kd_inclusion)->update(['KETERANGAN'=>$request->Inclusion]);
        if ($inclusion) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
    // fungsi menampilkan inclusion
    public function index()
    {
        $inclusion = Inclusion::all();

        if(count($inclusion) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $inclusion;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi menghapus inclusion
    public function delete(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_inclusion'                 => 'required',
            ]
            );
        $cari = InclusionDet::where('KD_INCLUSION',$request->Kd_inclusion)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $inclusion = Inclusion::where('KD_INCLUSION',$request->Kd_inclusion)->delete();
            if($inclusion)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,204);
            }
        }
    }
    // fungsi menambahkan inclusion detail untuk biro travel
    // public function storeInclusion(Request $request)
    // {
    //     $inclusion = DB::table('tb_inclusiondet')->insert($request);

    //     if($inclusion)
    //     { 
    //         $res['status'] = "Success";
    //         $res['data'] = "Data berhasil ditambahkan";
    //         return response($res);
    //     }
    //     else{
    //         $res['status'] = "Success";
    //         $res['data'] = "Data gagal ditambahkan";
    //         return response($res,200);
    //     }
    // }
}
