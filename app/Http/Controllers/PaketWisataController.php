<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaketWisata;
use App\Models\BiroTravel;
use App\Models\AktifitasDet;
use App\Models\InclusionDet;
use App\Models\Jadwal;
use App\Models\TransportasiDet;
use App\Models\DestinasiDetail;
use App\Models\Review;
use App\Models\Reservasi;
use App\Models\ObyekWisataDetail;
use App\Models\Transportasi;
use App\Models\Penginapan;
use App\Models\RencanaPerjalanan;
class PaketWisataController extends Controller
{
    public function obyekharga($id)
    {
        $obyek=ObyekWisataDetail::where('KD_OBYEKWISATADET',$id)->pluck('HARGA')->first();
        return $obyek;
    }
    public function transportasiharga($id)
    {
        $transportasi=Transportasi::where('KD_TRANSPORTASI',$id)->pluck('HARGA')->first();
        return $transportasi;
    }
    public function store(Request $request)
    {
        $this->validate(
            $request,[
                'Judul'                        => 'required',
                'Deskripsi'                    => 'required',
                'Total'                        => 'required',
                'Kd_akun'                      => 'required',
                'Kd_destinasi'                 => 'required',
                'Kd_penginapan'                => 'required',
                'Kd_tipewisata'                => 'required',
                'Status_pramuwisata'           => 'required',

            ]
            );
            $result = array();
            foreach ($request['Obyek Wisata'] as $obj2) {
                $json=substr($obj2['Kd_obyekwisatadet'], 0, 5);
                $row1=$this->obyekharga($json);
                array_push($result,$row1);
            }
            $totalobyek = array_sum($result);
            $result1 = array();
            foreach ($request['Transportasi'] as $obj2) {
                $json=substr($obj2['Kd_transportasi'], 0, 5);
                $row=$this->transportasiharga($json);
                array_push($result1,$row);
            }
            $totaltransportasi = array_sum($result1);
            $totalpenginapan = Penginapan::where('KD_PENGINAPAN',$request->Kd_penginapan)->pluck('HARGA')->first();
            $total = $totalobyek+$totaltransportasi+$totalpenginapan;
            $akun = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
            $paket = PaketWisata::create(
            [
                'JUDUL_PAKET'            =>$request->Judul,
                'DESKRIPSI_PAKET'        =>$request->Deskripsi,
                'TOTAL'                  =>$total,
                'KD_BIROTRAVEL'          =>$akun->KD_BIROTRAVEL,
                'KD_DESTINASI'           =>$request->Kd_destinasi,
                'KD_PENGINAPAN'          =>$request->Kd_penginapan,
                'KD_TIPEWISATA'          =>$request->Kd_tipewisata,
                'STATUS_PAKET'           =>1,
                'STATUS_PRAMUWISATA'     =>$request->Status_pramuwisata,
            
            ]
            );
            foreach ($request['Aktifitas'] as $obj) {
                $aktifitas = $paket->aktifitas=AktifitasDet::create(
                    [
                        'KD_PAKETWISATA'                  =>$paket->id,
                        'KD_AKTIFITAS'                    =>implode("|",$obj),
                    ]);
            }
            //$paket->aktifitas=AktifitasDet::insert($request['Aktifitas']);
            foreach ($request['Inclusion'] as $obj1) {
                $inclusion = $paket->inclusion=InclusionDet::create(
                    [
                        'KD_PAKETWISATA'                  =>$paket->id,
                        'KD_INCLUSION'                    =>implode("|",$obj1),
                    ]);
            }
            // $paket->inclusion=InclusionDet::insert($request['Inclusion']);
            foreach ($request['Obyek Wisata'] as $obj2) {
                $destinasi = $paket->destinasi=DestinasiDetail::create(
                    [
                        'KD_PAKETWISATA'                  =>$paket->id,
                        'KD_OBYEKWISATADET'               =>implode("|",$obj2),
                    ]);
            }
            // $paket->obyekwisata=DestinasiDet::insert($request['Obyek Wisata']);
            $jadwal=json_decode($request, true);
            foreach ($request->Jadwal as $obj3) {
                $json=substr($obj3['tgl_mulai'], 0, 10);
                $json1=substr($obj3['tgl_berakhir'], 0, 10);
                $json2=substr($obj3['status_jadwal'], 0, 5);
                $json3=substr($obj3['sisa_kuota'], 0, 5);
                $jadwal = $paket->jadwal=Jadwal::create(
                    
                    [
                        'KD_PAKETWISATA'                  =>$paket->id,
                        'TGL_MULAI'                       =>$json,
                        'TGL_BERAKHIR'                    =>$json1,
                        'STATUS_JADWAL'                   =>$json2,
                        'SISA_KUOTA'			  =>$json3,
                    ]);
            }
            // $paket->jadwal=Jadwal::insert($request['Jadwal']);
            foreach ($request['Transportasi'] as $obj4) {
                $transportasi = $paket->transportasi=TransportasiDet::create(
                    [
                        'KD_PAKETWISATA'                  =>$paket->id,
                        'KD_TRANSPORTASI'                 =>implode("|",$obj4),
                    ]);
            }
            $rencana=json_decode($request, true);
            foreach ($request->Rencana as $obj5) {
                $json=substr($obj5['keterangan'], 0, 9999);
                $json1=substr($obj5['harike-'], 0, 2);
                $rencana1 = $paket->rencana= RencanaPerjalanan::create(
                    [
                        'KD_PAKETWISATA'                    =>$paket->id,
                        'KETERANGAN'                      =>$json,
                        'HARIKE_'                         =>$json1,
                    ]);
            }

            // $paket->transportasi=TransportasiDet::insert($request['Transportasi']);
        if($paket && $transportasi && $jadwal && $destinasi && $inclusion && $aktifitas && $rencana1)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data berhasil ditambahkan";
            $res['test']= $rencana;
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi untuk menampilkan data paket wisata secara detail
    public function obyekwisata($paket)
    {
        $obyek = DestinasiDetail::Join('tb_obyekwisatadet', 'tb_detdestinasi.KD_OBYEKWISATADET', '=','tb_obyekwisatadet.KD_OBYEKWISATADET')
        ->Join('tb_obyekwisatamas', 'tb_obyekwisatamas.KD_OBYEKWISATAMAS', '=','tb_obyekwisatadet.KD_OBYEKWISATAMAS')
        ->select('tb_obyekwisatadet.KD_OBYEKWISATADET', 'tb_obyekwisatamas.NAMA_OBYEKWISATA','tb_obyekwisatadet.HARGA', 'tb_obyekwisatadet.FOTO_OBYEKWISATA')
        ->where('KD_PAKETWISATA', $paket)
        ->get();
        return $obyek;
    }
    public function transportasi($paket)
    {
        $transportasi = TransportasiDet::Join('tb_transportasi', 'tb_transportasidet.KD_TRANSPORTASI', '=','tb_transportasi.KD_TRANSPORTASI')
        ->Join('tb_jnstransportasi', 'tb_jnstransportasi.KD_JENISTRANSPORTASI', '=','tb_transportasi.KD_JENISTRANSPORTASI')
        ->select('tb_transportasi.NAMA_TRANSPORTASI', 'tb_transportasi.HARGA','tb_transportasi.FOTO_TRANSPORTASI', 'tb_jnstransportasi.NAMA_JENISTRANSPORTASI')
        ->where('KD_PAKETWISATA', $paket)
        ->get();
        return $transportasi;
    }
    public function jadwal($paket)
    {
        $jadwal = Jadwal::where('KD_PAKETWISATA', $paket)->get();
        return $jadwal;
    }
    public function review($paket)
    {
        $transportasi = Review::Join('tb_reservasi', 'tb_review.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
        ->Join('tb_wisatawan', 'tb_wisatawan.KD_WISATAWAN', '=','tb_reservasi.KD_WISATAWAN')
        ->Join('tb_akun', 'tb_akun.KD_AKUN', '=','tb_wisatawan.KD_AKUN')
        ->select('tb_reservasi.RESERVASI_ID', 'tb_review.RATING','tb_review.REVIEW','tb_wisatawan.NAMA_WISATAWAN','tb_akun.FOTO')
        ->where('KD_PAKETWISATA', $paket)
        ->get();
        return $transportasi;
    }
    public function index(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'      => 'required',
            ]
            );
        $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
        $paketwisata = PaketWisata::
        Join('tb_destinasi', 'tb_destinasi.KD_DESTINASI', '=','tb_paketwisata.KD_DESTINASI')
        ->Join('tb_penginapan', 'tb_penginapan.KD_PENGINAPAN', '=','tb_paketwisata.KD_PENGINAPAN')
        ->Join('tb_tipewisata', 'tb_tipewisata.KD_TIPEWISATA', '=','tb_paketwisata.KD_TIPEWISATA')
        ->select('tb_paketwisata.KD_PAKETWISATA', 'tb_paketwisata.JUDUL_PAKET','tb_paketwisata.DESKRIPSI_PAKET', 'tb_paketwisata.TOTAL', 'tb_paketwisata.STATUS_PRAMUWISATA'
        , 'tb_paketwisata.STATUS_PAKET', 'tb_destinasi.NAMA_DESTINASI', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_tipewisata.KETERANGAN')
        ->where('tb_paketwisata.KD_BIROTRAVEL', $biro->KD_BIROTRAVEL)->get();
        $result = array();
        foreach($paketwisata as $row)
        {
            $row['Obyek Wisata']=$this->obyekwisata($row['KD_PAKETWISATA']);
            $row['Transportasi']=$this->transportasi($row['KD_PAKETWISATA']);
            $row['Jadwal']=$this->jadwal($row['KD_PAKETWISATA']);
            $row['Review']=$this->review($row['KD_PAKETWISATA']);
            $rating=Review::Join('tb_reservasi', 'tb_review.KD_RESERVASI', '=','tb_reservasi.KD_RESERVASI')
            ->where('tb_reservasi.KD_PAKETWISATA',$row['KD_PAKETWISATA'])
            ->groupBy('tb_reservasi.KD_PAKETWISATA')
            ->avg('RATING');
            $row['Rating']=$rating;
            $row['Jml Perjalanan'] = Reservasi::where('STATUS_RESERVASI','Finish')->where('KD_PAKETWISATA',$row['KD_PAKETWISATA'])->count();
            array_push($result,$row);
        }
        
        if(count($result) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] =$result;
            return response($res);
            
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // menghapus paket wisata biro travel
    public function delete(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_reservasi'                 => 'required',
            ]
            );
        $cari = Reservasi::where('KD_PAKETWISATA',$request->Kd_reservasi)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $paketwisata = PaketWisata::where('KD_PAKETWISATA',$request->Kd_reservasi)->delete();
            $paketwisata = Jadwal::where('KD_PAKETWISATA',$request->Kd_reservasi)->delete();
            $paketwisata = TransportasiDet::where('KD_PAKETWISATA',$request->Kd_reservasi)->delete();
            $paketwisata = InclusionDet::where('KD_PAKETWISATA',$request->Kd_reservasi)->delete();
            $paketwisata = AktifitasDet::where('KD_PAKETWISATA',$request->Kd_reservasi)->delete();
            $paketwisata = RencanaPerjalanan::where('KD_PAKETWISATA',$request->Kd_reservasi)->delete();
            $paketwisata = DestinasiDetail::where('KD_PAKETWISATA',$request->Kd_reservasi)->delete();
            // $penginapan->delete();
            if($paketwisata)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,200);
            }
        }
    }
    // MENGUBAH JADWAL
    // public function update(Request $request)
    // {
    //     $this->validate(
    //         $request,[
    //             'Kd_inclusion'      => 'required',
    //             'Inclusion'         => 'required',
    //         ]
    //         );
    //     $inclusion = Jadwal::where('KD_JADWAL',$request->KD_JADWAL)->update(['TGL_MULAI'=>$request->TGL_MULAI,'TGL_BERAKHIR'=>$request->TGL_BERAKHIR]);
    //     if ($inclusion) 
    //     {
    //         $res['status'] = "Success";
    //         $res['pesan']= "Data telah berhasil diubah";
    //         return response($res,201);
    //     }
    //     else
    //     {
    //         $res['status'] = "Success";
    //         $res['pesan']= "tidak ada data yang diubah";
    //         return response($res,200);
    //     }

    // }
    
    // TAMBAH JADWAL
        public function tambah(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_paketwisata'      => 'required',
                'Tgl_mulai'         => 'required',
                'Tgl_berakhir'         => 'required',
                'Sisa_kuota'         => 'required',
            ]
            );
            $jadwal = Jadwal::create(
                [
                    'KD_PAKETWISATA'        => $request->Kd_paketwisata,
                    'TGL_MULAI'        => $request->Tgl_mulai,
                    'TGL_BERAKHIR'        => $request->Tgl_berakhir,
                    'SISA_KUOTA'        => $request->Sisa_kuota,
                ]
                );
        if ($jadwal) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
}
