<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipeWisata;
use App\Models\PaketWisata;

class TipeWisataController extends Controller
{
    // fungsi menambahkan tipe wisata admin
    public function store(Request $request)
    {
        $this->validate(
            $request,[
                'TipeWisata'             => 'required',
            ]
            );
            $tipewisata = TipeWisata::create(
            [
                'KETERANGAN'        => $request->TipeWisata,
            ]
            );
        if($tipewisata)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi mengubah tipe wisata admin
    public function update(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_tipe'            => 'required',
                'TipeWisata'         => 'required',
            ]
            );
        $tipewisata = TipeWisata::where('KD_TIPEWISATA',$request->Kd_tipe)->update(['KETERANGAN'=>$request->TipeWisata]);
        if ($tipewisata) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
    // fungsi menampilkan tipe wisata
    public function index()
    {
        $tipewisata = TipeWisata::all();

        if(count($tipewisata) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $tipewisata;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
        // menghapus tipe wisata
    public function delete(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_tipe'                 => 'required',
            ]
            );
        $cari = PaketWisata::where('KD_TIPEWISATA',$request->Kd_tipe)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $penginapan = TipeWisata::where('KD_TIPEWISATA',$request->Kd_tipe)->delete();
            if($penginapan)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,204);
            }
        }
    }
}
