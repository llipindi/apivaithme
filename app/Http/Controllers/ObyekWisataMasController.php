<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ObyekWisataMas;
use App\Models\ObyekWisataDetail;


class ObyekWisataMasController extends Controller
{
    public function index(Request $request)
    {
        // $this->validate(
        //     $request,[
        //         'Kd_destinasi'     => 'required',
        //     ]
        //     );
        $obyek = ObyekWisataMas::where('KD_DESTINASI', $request->Kd_destinasi)->get();

        if(count($obyek) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $obyek;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
}
