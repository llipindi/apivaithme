<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Review;
use App\Models\ReviewPemandu;

class ReviewController extends Controller
{
    // fungsi menambahkan review biro
    public function storebiro(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_reservasi'              => 'required',
                'Kd_wisatawan'              => 'required',
            ]
            );
            $obyek = Review::create(
            [
               'KD_WISATAWAN'       =>$request->Kd_wisatawan,
               'KD_RESERVASI'       =>$request->Kd_reservasi,
               'RATING'             =>$request->Rating,
               'REVIEW'             =>$request->Review,
            ]
            );
        if($obyek)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi menambahkan review pramuwisata
    public function storepramuwisata(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_reservasi'              => 'required',
                'Kd_wisatawan'              => 'required',
            ]
            );
            $obyek = ReviewPemandu::create(
            [
                'KD_WISATAWAN'       =>$request->Kd_wisatawan,
                'KD_RESERVASI'       =>$request->Kd_reservasi,
                'RATING'             =>$request->Rating,
                'REVIEW'             =>$request->Review,
            ]
            );
        if($obyek)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
}
