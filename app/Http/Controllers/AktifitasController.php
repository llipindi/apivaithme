<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aktifitas;
use App\Models\AktifitasDet;

class AktifitasController extends Controller
{
    // fungsi menambahkan aktifitas admin
    public function store(Request $request)
    {
        $this->validate(
            $request,[
                'Aktifitas'             => 'required',
            ]
            );
            $aktifitas = Aktifitas::create(
            [
                'NAMA_AKTIFITAS'        => $request->Aktifitas,
            ]
            );
        if($aktifitas)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi mengubah aktifitas admin
    public function update(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_Aktifitas'            => 'required',
                'Aktifitas'         => 'required',
            ]
            );
        $aktifitas = Aktifitas::where('KD_AKTIFITAS',$request->Kd_Aktifitas)->update(['NAMA_AKTIFITAS'=>$request->Aktifitas]);
        if ($aktifitas) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
    // fungsi menampilkan aktifitas
    public function index()
    {
        $aktifitas = Aktifitas::all();

        if(count($aktifitas) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $aktifitas;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi untuk menghapus aktifitas
    public function delete(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_aktifitas'                 => 'required',
            ]
            );
        $cari = AktifitasDet::where('KD_AKTIFITAS',$request->Kd_aktifitas)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $penginapan = Aktifitas::where('KD_AKTIFITAS',$request->Kd_aktifitas)->delete();
            if($penginapan)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,200);
            }
        }
    }
    // fungsi menambahkan aktifitas detail untuk biro travel
    // public function storeAktifitas(Request $request)
    // {
    //     $aktifitas = DB::table('tb_aktifitasdetail')->insert($request);

    //     if($aktifitas)
    //     { 
    //         $res['status'] = "Success";
    //         $res['data'] = "Data berhasil ditambahkan";
    //         return response($res);
    //     }
    //     else{
    //         $res['status'] = "Success";
    //         $res['data'] = "Data gagal ditambahkan";
    //         return response($res,200);
    //     }
    // }
}
