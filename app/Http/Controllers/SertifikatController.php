<?php

namespace App\Http\Controllers;
use App\Models\Sertifikat;
use Auth;

use Illuminate\Http\Request;

class SertifikatController extends Controller
{
    // fungsi menambahkan sertifikat
    public function store(Request $request)
    {
        $this->validate(
            $request,[
                'Domestik'             => 'required',
                'Url_sertifikat'       => 'required',
                'Kd_akun'              => 'required'
            ]
            );
            $sertifikat = Sertifikat::create(
            [
                'KD_AKUN'           =>$request->Kd_akun,
                'URL_SERTIFIKAT'    =>$request->Url_sertifikat,
                'DOMESTIK'          =>$request->Domestik,
            ]
            );
        if($sertifikat)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkans";
            return response($res,200);
        }
    }
    // fungsi melihat sertifikat
    public function index(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'     => 'required',
            ]
            );
        
        $sertifikat = Sertifikat::where('KD_AKUN', $request->Kd_akun)->get();
        if($sertifikat)
        {
            $res['status'] = "Success";
            $res['data']= $sertifikat;
            return response($res, 200);
        }
        else
        {
            $res['status'] = "Success";
            $res['data']= "tidak ada data";
            return response($res, 200);
        }
    }
    // fungsi mengubah sertifikat
    public function update(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_sertifikat'     => 'required',
                'Url'               => 'required',
                'Domestik'          => 'required',
            ]
            );
        $sertifikat = Sertifikat::where('KD_SERTIFIKAT',$request->Kd_sertifikat)->update(['URL_SERTIFIKAT'=>$request->Url, 'DOMESTIK'=>$request->Domestik]);
        if ($sertifikat) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
        }

    }
}
