<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penginapan;
use App\Models\JnsPenginapan;
use App\Models\BiroTravel;
use App\Models\PaketWisata;

class PenginapanController extends Controller
{
    // fungsi menambahkan jenis penginapan admin
    public function storejnspenginapan(Request $request)
    {
        $this->validate(
            $request,[
                'Jnspenginapan'              => 'required',
                'Deskripsi'                  => 'required',
            ]
            );
            $jnspenginapan = JnsPenginapan::create(
            [
                'NAMA_JENISPENGINAPAN'       =>$request->Jnspenginapan,
                'DESKRIPSI_JENISPENGINAPAN'=>$request->Deskripsi,
            ]
            );
        if($jnspenginapan)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi mengubah jenis penginapan admin
    public function updatejnspenginapan(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_jnspenginapan'      => 'required',
                'Jnspenginapan'         => 'required',
                'Deskripsi'             => 'required',
            ]
            );
        $jnspenginapan = JnsPenginapan::where('KD_JENISPENGINAPAN',$request->Kd_jnspenginapan)->update(['NAMA_JENISPENGINAPAN'=>$request->Jnspenginapan, 'DESKRIPSI_JENISPENGINAPAN'=>$request->Deskripsi]);
        if ($jnspenginapan) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
    // fungsi menampilkan jenis penginapan
    public function indexjnspenginapan()
    {
        $jnspenginapan = JnsPenginapan::all();

        if(count($jnspenginapan) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $jnspenginapan;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi menambahkan penginapan biro travel
    public function storepenginapan(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_jnspenginapan'              => 'required',
                'Kd_akun'                       => 'required',
                'Nama'                          => 'required',
                'Harga'                         => 'required',
                'Foto'                          => 'required',
            ]
            );
            $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
            $penginapan = Penginapan::create(
            [
                'KD_JENISPENGINAPAN'     =>$request->Kd_jnspenginapan,
                'KD_BIROTRAVEL'          =>$biro->KD_BIROTRAVEL,
                'NAMA_PENGINAPAN'        =>$request->Nama,
                'HARGA'                  =>$request->Harga,
                'FOTO_PENGINAPAN'        =>$request->Foto,
            ]
            );
        if($penginapan)
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah sukses ditambahkan";
            return response($res,201);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "Data gagal ditambahkan";
            return response($res,200);
        }
    }
    // fungsi menampilkan penginapan biro travel
    public function penginapan($id, $penginapan)
    {
        $penginapan = Penginapan::Join('tb_jnspenginapan', 'tb_jnspenginapan.KD_JENISPENGINAPAN', '=','tb_penginapan.KD_JENISPENGINAPAN')
            ->select('tb_penginapan.KD_PENGINAPAN', 'tb_jnspenginapan.NAMA_JENISPENGINAPAN', 'tb_penginapan.NAMA_PENGINAPAN', 'tb_penginapan.HARGA', 'tb_penginapan.FOTO_PENGINAPAN')
            ->where('tb_penginapan.KD_BIROTRAVEL', $id)
            ->where('tb_penginapan.KD_JENISPENGINAPAN', $penginapan)
            ->get();
            return $penginapan;
    }
    public function indexpenginapan(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_akun'      => 'required',
            ]
            );
        $biro = BiroTravel::where('KD_AKUN', $request->Kd_akun)->first();
        if($biro)
        {
            $penginapan = JnsPenginapan::all();
            $result = array();
            foreach($penginapan as $row)
            {
                $row['Penginapan']=$this->penginapan($biro->KD_BIROTRAVEL,$row['KD_JENISPENGINAPAN']);
                array_push($result,$row);
            }
        }
        else
        {
            $res['status'] = "Success";
            $res['data'] = "bukan biro travel";
            return response($res);
        }
        if(count($penginapan) > 0){ //mengecek apakah data kosong atau tidak
            $res['status'] = "Success";
            $res['data'] = $penginapan;
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data yang diminta tidak ada";
            return response($res,200);
        }
    }
    // fungsi mengubah penginapan biro travel
    public function updatepenginapan(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_penginapan'                 => 'required',
                'Kd_jnspenginapan'              => 'required',
                'Nama'                          => 'required',
                'Harga'                         => 'required',
                'Foto'                          => 'required',
            ]
            );
        $penginapan = Penginapan::where('KD_PENGINAPAN',$request->Kd_penginapan)->update(['NAMA_PENGINAPAN'=>$request->Nama, 'KD_JENISPENGINAPAN'=>$request->Kd_jnspenginapan, 'HARGA'=>$request->Harga, 'FOTO_PENGINAPAN'=>$request->Foto]);
        if ($penginapan) 
        {
            $res['status'] = "Success";
            $res['pesan']= "Data telah berhasil diubah";
            return response($res,200);
        }
        else
        {
            $res['status'] = "Success";
            $res['pesan']= "tidak ada data yang diubah";
            return response($res,200);
        }

    }
    // menghapus penginapan detail biro travel
    public function delete(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_penginapan'                 => 'required',
            ]
            );
        $cari = PaketWisata::where('KD_PENGINAPAN',$request->Kd_penginapan)->select('*')->count();
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $penginapan = Penginapan::where('KD_PENGINAPAN',$request->Kd_penginapan)->delete();
            // $penginapan->delete();
            if($penginapan)
            {
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,200);
            }
        }
    }
    // menghapus jenis penginapan biro travel
    public function deletejns(Request $request)
    {
        $this->validate(
            $request,[
                'Kd_jnspenginapan'                 => 'required',
            ]
            );
        $cari = Penginapan::where('KD_JENISPENGINAPAN',$request->Kd_jnspenginapan)->select('*')->count();
        
        if($cari>0)
        {
            $res['status'] = "Success";
            $res['pesan']= "data tidak dapat dihapus";
            return response($res,200);
        }
        else
        {
            $penginapan = JnsPenginapan::where('KD_JENISPENGINAPAN',$request->Kd_jnspenginapan)->delete();
           //echo 'test';
            if($penginapan)
            {
            //echo 'test1';
                $res['status'] = "Success";
                $res['pesan']= "data dihapus";
                return response($res,201);
            }
        }
    }
}
