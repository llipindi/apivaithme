<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Mobile\Akun;
use App\Models\Mobile\Jadwal;
use App\Models\Mobile\ObyekWisata;
use App\Models\Mobile\ObyekWisataPaket;
use App\Models\Mobile\PaketWisata;
use App\Models\Mobile\Penginapan;
use App\Models\Mobile\Reservasi;
use App\Models\Mobile\Review;
use App\Models\Mobile\ReviewPemanduWisata;
use App\Models\Mobile\TipeWisata;
use App\Models\Mobile\Transportasi;
use App\Models\Mobile\TransportasiWisata;
use App\Models\Mobile\Wisatawan;
use App\Models\SaldoBiro;
use App\Models\SaldoPemandu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    public function review(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kd_wisatawan' => 'required',
            'kd_reservasi' => 'required',
            'rating' => 'required',
            'review' => 'required'
        ]);

        if ($validator->fails()) {
            return json_response(null, 401, implode(",", $validator->messages()->all()), true);
        }

        $review = ReviewPemanduWisata::create([
            'KD_WISATAWAN' => Input::get('kd_wisatawan'),
            'KD_RESERVASI' => Input::get('kd_reservasi'),
            'RATING' => Input::get('rating'),
            'REVIEW' => Input::get('review'),
            'TGL_UPDATE' => Carbon::now()
        ]);

        Review::create([
            'KD_WISATAWAN' => Input::get('kd_wisatawan'),
            'KD_RESERVASI' => Input::get('kd_reservasi'),
            'RATING' => Input::get('rating'),
            'REVIEW' => Input::get('review'),
            'TGL_UPDATE' => Carbon::now()
        ]);

        if ($review) {
            Reservasi::where('KD_RESERVASI', $request->get("kd_reservasi"))->update(['STATUS_RESERVASI' => 7]);
            $r=Reservasi::with(['paket_wisata_detail'])->where('KD_RESERVASI', $request->get("kd_reservasi"))->first();
            SaldoBiro::create([
                'KD_BIROTRAVEL' => $r->paket_wisata_detail->KD_BIROTRAVEL,
                'TOTAL' => $r->TOTAL_PAKET,
                'TGL_UPDATE' => Carbon::now()
            ]);
            SaldoPemandu::create([
                'KD_PEMANDUWISATA' => $r->KD_PEMANDUWISATA,
                'TOTAL' => $r->TOTAL_PRAMUWISATA,
                'TGL_UPDATE' => Carbon::now()
            ]);
            return json_response(['message' => 'review berhasil'], 200);
        } else {
            return json_response(null, 401, "Error simpan reservasi", true);
        }
    }
}