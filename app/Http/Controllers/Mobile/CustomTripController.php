<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Mobile\Akun;
use App\Models\Mobile\BiroTravel;
use App\Models\Mobile\CriteriaWP;
use App\Models\Mobile\Destinasi;
use App\Models\Mobile\ObyekWisata;
use App\Models\Mobile\PaketWisata;
use App\Models\Mobile\Penginapan;
use App\Models\Mobile\TipeWisata;
use App\Models\Mobile\Transportasi;
use App\Models\Mobile\Wisatawan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class CustomTripController extends Controller
{
    function quick_sort($array)
    {
        // find array size
        $length = count($array);

        // base case test, if array of length 0 then just return array to caller
        if ($length <= 1) {
            return $array;
        } else {

            // select an item to act as our pivot point, since list is unsorted first position is easiest
            $pivot = $array[0];

            // declare our two arrays to act as partitions
            $left = $right = array();

            // loop and compare each item in the array to the pivot value, place item in appropriate partition
            for ($i = 1; $i < count($array); $i++) {
                if ($array[$i]["V"] > $pivot["V"]) {
                    $left[] = $array[$i];
                } else {
                    $right[] = $array[$i];
                }
            }

            // use recursion to now sort the left and right lists
            return array_merge($this->quick_sort($left), array($pivot), $this->quick_sort($right));
        }
    }

    public function obyek_wisata($kd_birotravel, $kd_destinasi)
    {
        $data = ObyekWisata::
        leftJoin('tb_obyekwisatamas', 'tb_obyekwisatadet.KD_OBYEKWISATAMAS', '=', 'tb_obyekwisatamas.KD_OBYEKWISATAMAS')
            ->where('kd_birotravel', $kd_birotravel)->where('kd_destinasi', $kd_destinasi)->get();
        return json_response($data, 200);
    }

    public function penginapan($kd_birotravel)
    {
        $data = Penginapan::where('KD_BIROTRAVEL', $kd_birotravel)->get();
        return json_response($data, 200);
    }

    public function transportasi($kd_birotravel)
    {
        $data = Transportasi::where('KD_BIROTRAVEL', $kd_birotravel)->get();
        return json_response($data, 200);
    }

    public function weighted_product()
    {
        /*
        * *********************************************
        *  GET ALL BIRO TRAVEL BY FILTER AND PUT AS ALTERNATIVES
        *  FILTER : DESTINASI WISATA
        * *********************************************
        */
        $kd_destinasi = 0;
        $d = Destinasi::whereRaw('lower(NAMA_DESTINASI) LIKE "%' . strtolower(Input::get('destinasi')) . '%"')->get();
        // dd($d);
        if ($d->count() >= 1) {
            $kd_destinasi = $d[0]->KD_DESTINASI;
        } else {
            return json_response(null, 401, "Destinasi wisata tidak ditemukan, periksa kembali tujuan anda", true);
        }
        $alternatives = $this->get_alternatives($kd_destinasi);

        /*
         * *********************************************
         * GET CRITERIA and BOBOT
         * *********************************************
         */
        $criteria = CriteriaWP::orderBy('cid')->get();

        /*
        * *********************************************
        * VEKTOR S
        * *********************************************
        */
        $total_s = 0;
        for ($i = 0; $i < count($alternatives); $i++) {
            $a = $alternatives[$i];
            $C1 = 0;
            $C2 = 0;
            $C3 = 0;
            $C4 = 0;
            $C5 = 0;
            foreach ($criteria as $c) {
                // kriteria ke- C1, C2, C3, dst.
                $power = ($c->type == 1) ? $c->perbaikan_bobot : ($c->perbaikan_bobot * -1);
                switch ($c->cid) {
                    case "C1":
                        $C1 = pow($a['rating'], $power);
                        break;
                    case "C2":
                        $C2 = pow($a['budget'], $power);
                        break;
                    case "C3":
                        $C3 = pow($a['jumlah_wisata'], $power);
                        break;
                    case "C4":
                        $C4 = pow($a['jumlah_penginapan'], $power);
                        break;
                    case "C5":
                        $C5 = pow($a['jumlah_transportasi'], $power);
                        break;
                }
            }
            $a["CS"] = [(String)(round($C1, 8)), (String)(round($C2, 8)), (String)(round($C3, 8)), (String)(round($C4, 8)), (String)(round($C5, 8))];
            $s = ($C1 * $C2 * $C3 * $C4 * $C5);
            // total s buat pembagi vektor v nanti
            $total_s += $s;
            $a["S"] = (String)(round($s, 8));
            $alternatives[$i] = $a;
        }

        /*
        * *********************************************
        * VEKTOR V
        * *********************************************
        */
        for ($i = 0; $i < count($alternatives); $i++) {
            $a = $alternatives[$i];
            $v = $a["S"] / $total_s;
            $a["V"] = (String)(round($v, 8));
            $alternatives[$i] = $a;
        }

//        dd($total_s);

        /*
        * *********************************************
        * SORT BY VEKTOR V
        * *********************************************
        */
        $alternatives = $this->quick_sort($alternatives);
        return json_response(["destinasi" => $d[0], "wp_result" => $alternatives], 200);
    }

    public function get_alternatives($kd_destinasi)
    {
        $data = PaketWisata::selectRaw('kd_birotravel')->where('kd_destinasi', $kd_destinasi)->groupBy('KD_BIROTRAVEL')->get();
        $alternatif = [];
        foreach ($data as $b) {
            $biro = BiroTravel::find($b->kd_birotravel);

            /*
             * *********************************************
             *  GET JUMLAH WISATA
             * *********************************************
             */
            $wisata = ObyekWisata::
            leftJoin('tb_obyekwisatamas', 'tb_obyekwisatadet.KD_OBYEKWISATAMAS', '=', 'tb_obyekwisatamas.KD_OBYEKWISATAMAS')
                ->where('kd_birotravel', $biro->KD_BIROTRAVEL)->where('kd_destinasi', $kd_destinasi)->get();
            $jumlah_wisata = $wisata->count();

            /*
             * *********************************************
             *  GET JUMLAH PENGINAPAN
             * *********************************************
             */
            $jumlah_penginapan = Penginapan::where('kd_birotravel', $biro->KD_BIROTRAVEL)->count();

            /*
             * *********************************************
             *  GET JUMLAH TRANSPORTASI
             * *********************************************
             */
            $jumlah_transportasi = Transportasi::where('kd_birotravel', $biro->KD_BIROTRAVEL)->count();

            /*
             * *********************************************
             *  GET RATING
             * *********************************************
             */
            $rating = 0.1;
            $rt = DB::select(DB::raw(
                'SELECT re.RATING, p.KD_BIROTRAVEL FROM tb_review as re LEFT JOIN tb_reservasi as r on (re.KD_RESERVASI = r.KD_RESERVASI) LEFT JOIN tb_paketwisata as p ON (r.KD_PAKETWISATA = p.KD_PAKETWISATA) WHERE p.KD_BIROTRAVEL = :kd_biro'
            )
                , array("kd_biro" => $biro->KD_BIROTRAVEL));
            if (count($rt) > 1) {
                $total = 0;
                foreach ($rt as $r) {
                    $total += $r->RATING;
                }
                $rating = $total / count($rt);
            }

            /*
             * *********************************************
             *  GET RATA RATA BUDGET
             * *********************************************
             */
            $budget = 0;
            foreach ($wisata as $w) {
                $budget += $w->HARGA;
            }
            $budget = ceil($budget / $wisata->count());

            /*
             * *********************************************
             *  PUT ALTERNATIVES
             * *********************************************
             */
            $alternatif[] = [
                "kd_birotravel" => $biro->KD_BIROTRAVEL,
                "birotravel" => $biro,
                "jumlah_wisata" => $jumlah_wisata,
                "jumlah_penginapan" => $jumlah_penginapan,
                "jumlah_transportasi" => $jumlah_transportasi,
                "rating" => (String)(round(floatval($rating), 2)),
                "budget" => $budget,
            ];
        }
        return $alternatif;
    }
}