<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Mobile\Akun;
use App\Models\Mobile\Wisatawan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AkunController extends Controller
{
    public $successStatus = 200;

    public function login()
    {
        if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
            $user = Auth::user();
            $tokenResult = $user->createToken('vaithme');
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addDays(3);
            $token->save();
            $data = [
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_in' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'wisatawan' => Wisatawan::where("KD_AKUN", Auth::user()->KD_AKUN)->first()
            ];
            return json_response($data, 200);
        } else {
            return json_response(null, 401, "Authentication Falied", true);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return json_response(null, 401, $validator->errors(), true);
        }

        // CHECK IF USER EXISTS
        if (Akun::where('email', Input::get('email'))->count() >= 1) {
            // AKUN EXISTS
            return json_response(null, 401, "Email already registered", true);
        }

        $user = Akun::create([
            'EMAIL' => Input::get('email'),
            'USERNAME' => "",
            'PASSWORD' => bcrypt(Input::get('password')),
            'STATUS' => "Verifikasi",
            'TGL_UPDATE' => Carbon::now(),
            'FOTO' => "",
            'KD_TIPEAKUN' => "KTA-0003"
        ]);

        $wisatawan = Wisatawan::create([
            'KD_AKUN' => $user->KD_AKUN,
            'NAMA_WISATAWAN' => Input::get('nama'),
            'TL_WISATAWAN' => "1999-01-01",
            'NOTELP_WISATAWAN' => "",
            'JK_WISATAWAN' => "",
            'ALAMAT_WISATAWAN' => "",
            'TGL_UPDATE' => Carbon::now()
        ]);

        Auth::login($user);

        $tokenResult = Auth::user()->createToken('vaithme');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addDays(3);
        $token->save();
        $data = [
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'wisatawan' => Wisatawan::where("KD_AKUN", Auth::user()->KD_AKUN)->first()
        ];

        return json_response($data, 200);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return json_response(['message' => 'Logout Success'], 200);
    }

    public function wisatawan($kd_wisatawan)
    {
        $data = Wisatawan::find($kd_wisatawan);
        return json_response($data, 200);
    }

    public function update_profile(Request $request)
    {
        $rules = array(
            'kd_wisatawan' => 'required',
            'nama_wisatawan' => 'required',
            'tl_wisatawan' => 'required',
            'notelp_wisatawan' => 'required',
            'jk_wisatawan' => 'required',
            'alamat_wisatawan' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
            // |max:2048'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_response(null, 401, implode(",", $validator->messages()->all()), true);
        } else {
            $kd_wisatawan = Input::get('kd_wisatawan');
            $u_foto = Input::get('u_foto');
            // UPLOAD FOTO
            if ($u_foto == "true") {
                $image = $request->file("image");
                $imgname = time() . '.' . $image->getClientOriginalExtension();;
                $destinationPath = public_path('images/wisatawan/');
                $image->move($destinationPath, $imgname);
            }
            // SAVE TO DB
            $wisatawan = Wisatawan::where('KD_WISATAWAN', $kd_wisatawan)->update([
                // 'KD_AKUN' => $user->KD_AKUN,
                'NAMA_WISATAWAN' => Input::get('nama_wisatawan'),
                'TL_WISATAWAN' => Input::get('tl_wisatawan'),
                'NOTELP_WISATAWAN' => Input::get('notelp_wisatawan'),
                'JK_WISATAWAN' => Input::get('jk_wisatawan'),
                'ALAMAT_WISATAWAN' => Input::get('alamat_wisatawan'),
                'TGL_UPDATE' => Carbon::now()
            ]);
            if ($wisatawan) {
                $akun = true;
                if ($u_foto == "true") {
                    // UPDATE FOTO AKUN
                    $akun = Akun::where("kd_akun", Wisatawan::where('KD_WISATAWAN', $kd_wisatawan)->first()->KD_AKUN)->update([
                        'FOTO' => image_path('wisatawan') . $imgname
                    ]);
                }
                if ($akun) {
                    $data = [
                        'access_token' => '-',
                        'token_type' => '-',
                        'expires_at' => '-',
                        'wisatawan' => Wisatawan::where("KD_WISATAWAN", $kd_wisatawan)->first()
                    ];
                    return json_response($data, 200);
                } else {
                    return json_response(null, 401, "Error simpan akun", true);
                }
                // return json_response(['message' => 'Upload bukti pembayaran berhasil'], 200);
            } else {
                return json_response(null, 401, "Error simpan wisatawan", true);
            }
        }
    }
}