<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Mobile\Akun;
use App\Models\Mobile\Jadwal;
use App\Models\Mobile\ObyekWisata;
use App\Models\Mobile\ObyekWisataPaket;
use App\Models\Mobile\PaketWisata;
use App\Models\Mobile\Penginapan;
use App\Models\Mobile\Reservasi;
use App\Models\Mobile\TipeWisata;
use App\Models\Mobile\Transportasi;
use App\Models\Mobile\TransportasiWisata;
use App\Models\Mobile\Wisatawan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ReservasiController extends Controller
{
    public function history_by_wisatawan($kd_wisatawan)
    {
        $data = Reservasi::with(['paket_wisata_list', 'wisatawan'])->where('KD_WISATAWAN', $kd_wisatawan)->orderBy('KD_RESERVASI', 'DESC')->get();
        return json_response($data, 200);
    }

    public function history_by_id($kd_reservasi)
    {
        $data = Reservasi::with(['paket_wisata_detail', 'wisatawan', 'pemandu_wisata', 'review'])->where('KD_RESERVASI', $kd_reservasi)->orderBy('KD_RESERVASI', 'DESC')->get();
        return json_response($data, 200);
    }

    public function reservasi_id()
    {
        // GET LAST RESERVASI ID FROM TODAY
        $riq = DB::select(
            DB::raw('select RESERVASI_ID as r from tb_reservasi WHERE `RESERVASI_ID` like :rid ORDER BY RESERVASI_ID DESC LIMIT 0,1')
            , array("rid" => Carbon::now()->format("%VdmY%")));
        $rnew = "";
        if (count($riq) >= 1) {
            $rold = $riq[0]->r;
            $rold = substr($rold, -3);
            $rold = (int)$rold;
            $rnew = ($rold + 1);
            if ($rnew < 10) {
                $rnew = "00" . $rnew;
            } else if ($rnew < 100 && $rnew >= 10) {
                $rnew = "0" . $rnew;
            } else {
                $rnew = $rnew;
            }
        } else {
            $rnew = "001";
        }
        return Carbon::now()->format("VdmY") . $rnew;
    }

    public function opentrip_insert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kd_wisatawan' => 'required',
            'kd_paketwisata' => 'required',
            'tgl_perjalanan' => 'required',
            'total_pax' => 'required',
            'durasi' => 'required',
            'kd_jadwal' => 'required'
        ]);

        if ($validator->fails()) {
            return json_response(null, 401, implode(",", $validator->messages()->all()), true);
        }

        $total_paket = Input::get('total_paket');
        $total_pramuwisata = Input::get('total_pramuwisata');
        $total_tagihan = $total_paket + $total_pramuwisata;

        $reservasi = Reservasi::create([
            'KD_WISATAWAN' => Input::get('kd_wisatawan'),
            'KD_PEMANDUWISATA' => 0,
            'KD_PAKETWISATA' => Input::get('kd_paketwisata'),
            'TGL_PERJALANAN' => Input::get('tgl_perjalanan'),
            'TOTAL_PAX' => Input::get('total_pax'),
            'TOTAL_PAKET' => $total_paket,
            'TOTAL_PRAMUWISATA' => $total_pramuwisata,
            'TOTAL_TAGIHAN' => $total_tagihan,
            'TGL_RESERVASI' => Carbon::now(),
            'TGL_UPDATE' => Carbon::now(),
            'STATUS_RESERVASI' => 1,
            'RENCANA_PERJALANAN' => ' - ',
            'DURASI' => Input::get('durasi'),
            'RESERVASI_ID' => $this->reservasi_id()
        ]);
        if ($reservasi) {
            // UPDATE JADWAL
            Jadwal::where('KD_JADWAL', Input::get('kd_jadwal'))->decrement("SISA_KUOTA", Input::get('total_pax'));
            return json_response(['message' => 'reservasi berhasil'], 200);
        } else {
            return json_response(null, 401, "Error simpan reservasi", true);
        }
    }

    public function customtrip_insert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'total_paket' => 'required',
            'kd_birotravel' => 'required',
            'kd_destinasi' => 'required',
            'kd_penginapan' => 'required',
            'wisata' => 'required',
            'transportasi' => 'required',
            'total_paket_pax' => 'required',
            'total_pramuwisata' => 'required',
            'kd_wisatawan' => 'required',
            'tgl_perjalanan' => 'required',
            'total_pax' => 'required',
            'durasi' => 'required'
        ]);

        if ($validator->fails()) {
            return json_response(null, 401, implode(",", $validator->messages()->all()), true);
        }

        $paket = PaketWisata::create([
            'JUDUL_PAKET' => Input::get('judul'),
            'DESKRIPSI_PAKET' => "-",
            'TOTAL' => Input::get('total_paket'),
            'KD_BIROTRAVEL' => Input::get('kd_birotravel'),
            'KD_DESTINASI' => Input::get('kd_destinasi'),
            'KD_PENGINAPAN' => Input::get('kd_penginapan'),
            'KD_TIPEWISATA' => 0,
            'STATUS_PAKET' => 2,
            'STATUS_PRAMUWISATA' => 'YA'
        ]);

        // INSERT JADWAL
        Jadwal::create([
            'TGL_MULAI' => Input::get('tgl_perjalanan'),
            'TGL_BERAKHIR' => Carbon::parse(Input::get('tgl_perjalanan'))->addDays((Input::get('durasi') - 1)),
            'KD_PAKETWISATA' => $paket->KD_PAKETWISATA,
            'STATUS_JADWAL' => 'Aktif',
            'SISA_KUOTA' => 999
        ]);

        if ($paket) {
            // DESTINASI
            $wisata = explode(",", Input::get('wisata'));
            foreach ($wisata as $w) {
                ObyekWisataPaket::create([
                    "KD_PAKETWISATA" => $paket->KD_PAKETWISATA,
                    "KD_OBYEKWISATADET" => $w
                ]);
            }
            // TRANSPORTASI DETAIL
            $transport = explode(",", Input::get('transportasi'));
            foreach ($transport as $t) {
                TransportasiWisata::create([
                    "KD_PAKETWISATA" => $paket->KD_PAKETWISATA,
                    "KD_TRANSPORTASI" => $t
                ]);
            }

            // DO RESERVASI
            $total_paket = Input::get('total_paket_pax');
            $total_pramuwisata = Input::get('total_pramuwisata');
            $total_tagihan = $total_paket + $total_pramuwisata;

            $reservasi = Reservasi::create([
                'KD_WISATAWAN' => Input::get('kd_wisatawan'),
                'KD_PEMANDUWISATA' => 0,
                'KD_PAKETWISATA' => $paket->KD_PAKETWISATA,
                'TGL_PERJALANAN' => Input::get('tgl_perjalanan'),
                'TOTAL_PAX' => Input::get('total_pax'),
                'TOTAL_PAKET' => $total_paket,
                'TOTAL_PRAMUWISATA' => $total_pramuwisata,
                'TOTAL_TAGIHAN' => $total_tagihan,
                'TGL_RESERVASI' => Carbon::now(),
                'TGL_UPDATE' => Carbon::now(),
                'STATUS_RESERVASI' => 1,
                'RENCANA_PERJALANAN' => ' - ',
                'DURASI' => Input::get('durasi'),
                'RESERVASI_ID' => $this->reservasi_id()
            ]);
            if ($reservasi) {
                return json_response(['message' => 'reservasi berhasil'], 200);
            } else {
                return json_response(null, 401, "Error simpan reservasi", true);
            }
        } else {
            return json_response(null, 401, "INSERT PAKET FAILED", true);
        }
    }
}