<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Mobile\Akun;
use App\Models\Mobile\PaketWisata;
use App\Models\Mobile\TipeWisata;
use App\Models\Mobile\Wisatawan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PaketWisataController extends Controller
{
    public function tipe_wisata()
    {
        $data = TipeWisata::all();
        return json_response($data, 200);
    }

    public function paket_wisata()
    {
        $data = PaketWisata::with(['destinasi', 'tipe_wisata', 'obyek_wisata_paket'])->where('STATUS_PAKET', 1)->get();
        return json_response($data, 200);
    }

    public function paket_wisata_by_tipe($kd_tipewisata)
    {
        $data = PaketWisata::with(['destinasi', 'tipe_wisata', 'obyek_wisata_paket'])->where('KD_TIPEWISATA', $kd_tipewisata)->where('STATUS_PAKET', 1)->get();
        return json_response($data, 200);
    }

    public function paket_wisata_by_id($kd_paketwisata)
    {
        $data = PaketWisata::with(['tipe_wisata', 'destinasi', 'obyek_wisata_paket', 'penginapan', 'transportasi_wisata', 'birotravel', 'jadwal', 'inclusion', 'aktifitas_wisata', 'rencana_perjalanan'])->where('KD_PAKETWISATA', $kd_paketwisata)->get();
        return json_response($data, 200);
    }

    public function paket_wisata_search($query)
    {
        $query = strtolower($query);
        $data = PaketWisata::with(['destinasi', 'tipe_wisata', 'obyek_wisata_paket'])->leftJoin('tb_destinasi', 'tb_paketwisata.KD_DESTINASI', '=', 'tb_destinasi.KD_DESTINASI')->whereRaw('LOWER(tb_destinasi.NAMA_DESTINASI) LIKE "%' . $query . '%"')->get();
        return json_response($data, 200);
    }
}