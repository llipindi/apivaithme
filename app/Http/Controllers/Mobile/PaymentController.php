<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Mobile\Akun;
use App\Models\Mobile\ObyekWisata;
use App\Models\Mobile\PaketWisata;
use App\Models\Mobile\Penginapan;
use App\Models\Mobile\Reservasi;
use App\Models\Mobile\Tagihan;
use App\Models\Mobile\TipeWisata;
use App\Models\Mobile\Transportasi;
use App\Models\Mobile\Wisatawan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    public function upload(Request $request)
    {
        $rules = array(
            'kd_reservasi' => 'required',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
            // |max:2048'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_response(null, 401, implode(",", $validator->messages()->all()), true);
        } else {
            // UPLOAD FOTO
            $image = $request->file("image");
            $imgname = time() . '.' . $image->getClientOriginalExtension();;
            $destinationPath = public_path('images/tagihan/');
            $image->move($destinationPath, $imgname);
            // SAVE TO DB
            $tagihan = Tagihan::create([
                'KD_RESERVASI' => Input::get("kd_reservasi"),
                'KD_ADMIN' => 0,
                'STATUS_TAGIHAN' => '1',
                'FOTO_TRANSFER' => image_path('tagihan') . $imgname,
                'TGL_UPDATE' => Carbon::now()
            ]);
            if ($tagihan) {
                Reservasi::where('KD_RESERVASI', $request->get("kd_reservasi"))->update(['STATUS_RESERVASI' => 5]);
                return json_response(['message' => 'Upload bukti pembayaran berhasil'], 200);
            } else {
                return json_response(null, 401, "Error simpan pembayaran", true);
            }
        }
    }
}