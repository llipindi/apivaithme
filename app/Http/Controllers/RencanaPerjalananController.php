<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RencanaPerjalanan;
use App\Models\Reservasi;

class RencanaPerjalananController extends Controller
{
    // fungsi menambahkan renper
    public function storeRencana(Request $request)
    {
        $inclusion = RencanaPerjalanan::insert($request);
        $reservasi = Reservasi::where('KD_INCLUSION',$request->Kd_inclusion)->update(['KETERANGAN'=>$request->Inclusion]);

        if($inclusion && $reservasi)
        { 
            $res['status'] = "Success";
            $res['data'] = "Data berhasil ditambahkan";
            return response($res);
        }
        else{
            $res['status'] = "Success";
            $res['data'] = "Data gagal ditambahkan";
            return response($res,200);
        }
    }
}
