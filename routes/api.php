<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * TODO ROUTES DI BIKIN PAKE MIDDLEWARE CLIENT SAMA PASSWORD
 *
*/
Route::group(['prefix' => 'v1'], function () {
    // BUAT KEPERLUAN WEB
    Route::post('/Akun', 'AkunController@index');
    Route::post('/Register', 'AkunController@store');
    Route::post('/Login', 'AkunController@login');
    Route::post('/CekEmail', 'AkunController@cekemail');
    Route::post('/CekUsername', 'AkunController@cekusername');
    Route::post('/GetProfil', 'AkunController@getprofil');
    Route::put('/UbahStatus', 'AkunController@update');
    Route::put('/UbahProfil', 'AkunController@ubahprofil');
    //dashboard
    Route::get('/HitungAkun', 'AkunController@countaccount');
    //rekening
    Route::post('/TambahRekening', 'RekeningController@store');
    Route::post('/Rekening', 'RekeningController@index');
    Route::put('/UbahRekening', 'RekeningController@update');
    //sertifikat
    Route::post('/TambahSertifikat', 'SertifikatController@store');
    Route::post('/Sertifikat', 'SertifikatController@index');
    Route::post('/UbahSertifikat', 'SertifikatController@update');
    // jns penginapan
    Route::get('/JnsPenginapan', 'PenginapanController@indexjnspenginapan');
    Route::post('/TambahJnsPenginapan', 'PenginapanController@storejnspenginapan');
    Route::put('/UbahJnsPenginapan', 'PenginapanController@updatejnspenginapan');
    Route::delete('/HapusJnsPenginapan', 'PenginapanController@deletejns');
    // penginapan
    Route::post('/Penginapan', 'PenginapanController@indexpenginapan');
    Route::post('/TambahPenginapan', 'PenginapanController@storepenginapan');
    Route::put('/UbahPenginapan', 'PenginapanController@updatepenginapan');
    Route::delete('/HapusPenginapan', 'PenginapanController@delete');

    // jns transportasi
    Route::get('/JnsTransportasi', 'TransportasiController@indexjnstransportasi');
    Route::post('/TambahJnsTransportasi', 'TransportasiController@storejnstransportasi');
    Route::put('/UbahJnsTransportasi', 'TransportasiController@updatejnstransportasi');
    Route::delete('/HapusJnsTransportasi', 'TransportasiController@deletejns');
    // transportasi
    Route::post('/Transportasi', 'TransportasiController@indextransportasi');
    Route::post('/TambahTransportasi', 'TransportasiController@storetransportasi');
    Route::put('/UbahTransportasi', 'TransportasiController@updatetransportasi');
    Route::delete('/HapusTransportasi', 'TransportasiController@delete');
    // destinasi
    Route::get('/Destinasi', 'DestinasiController@index');
    Route::post('/TambahDestinasi', 'DestinasiController@store');
    Route::post('/UbahDestinasi', 'DestinasiController@update');
    Route::delete('/HapusDestinasi', 'DestinasiController@delete');
    // obyek wisata master
    Route::post('/ObyekWisataMas', 'ObyekWisataController@index');
    Route::post('/TambahObyekWisataMas', 'ObyekWisataController@store');
    Route::post('/UbahObyekWisataMas', 'ObyekWisataController@update');
    Route::delete('/HapusObyekWisataMas', 'ObyekWisataController@delete');
    // obyek wisata detail
    Route::post('/ObyekWisataDet', 'ObyekWisataController@indexObyekWisataDet');
    Route::post('/ListObyekWisataDet', 'ObyekWisataController@indexlistobyekwisatadet');
    Route::post('/TambahObyekWisataDet', 'ObyekWisataController@storeObyekWisataDet');
    Route::put('/UbahObyekWisataDet', 'ObyekWisataController@updateObyekWisataDet');
    Route::delete('/HapusObyekWisataDet', 'ObyekWisataController@deletejns');
    // tipe wisata
    Route::get('/TipeWisata', 'TipeWisataController@index');
    Route::post('/TambahTipeWisata', 'TipeWisataController@store');
    Route::put('/UbahTipeWisata', 'TipeWisataController@update');
    Route::delete('/HapusTipeWisata', 'TipeWisataController@delete');
    // aktifitas
    Route::get('/Aktifitas', 'AktifitasController@index');
    Route::post('/TambahAktifitas', 'AktifitasController@store');
    Route::post('/UbahAktifitas', 'AktifitasController@update');
    Route::delete('/HapusAktifitas', 'AktifitasController@delete');
    // rencana perjalanan
    // Route::get('/JnsObyekWisataMas', 'ObyekWisataController@index');
    // Route::post('/TambahObyekWisataMas', 'ObyekWisataController@store');
    // Route::put('/UbahObyekWisataMas', 'ObyekWisataController@update');
    // inclusion
    Route::get('/Inclusion', 'InclusionController@index');
    Route::post('/TambahInclusion', 'InclusionController@store');
    Route::post('/UbahInclusion', 'InclusionController@update');
    Route::delete('/HapusInclusion', 'InclusionController@delete');
    //paket wisata
    Route::post('/TambahPaketWisata', 'PaketWisataController@store');
    Route::post('/PaketWisataBiro', 'PaketWisataController@index');
    Route::delete('/HapusPaketWisata', 'PaketWisataController@delete');
    //reservasi
    Route::post('/Order', 'ReservasiController@index');
    Route::post('/Riwayat_reservasi', 'ReservasiController@riwayat_jadwal');
    Route::post('/Jadwal_reservasi', 'ReservasiController@jadwal_reservasi');
    Route::post('/Konfirmasi', 'ReservasiController@update');
    Route::post('/SemuaPerjalanan', 'ReservasiController@allperjalanan');
    Route::post('/TambahRenpen', 'ReservasiController@create');
    Route::post('/TambahReservasi', 'ReservasiController@store');
    //review
    Route::post('/TambahReview', 'ReviewController@storebiro');
    Route::post('/TambahReviewPemandu', 'ReviewController@storepramuwisata');
    //pembayaran
    Route::post('/IndexPembayaran', 'ReservasiController@indexpembayaran');
    Route::post('/IndexPembayaranKurang', 'ReservasiController@indexpembayarankurang');
    Route::post('/IndexPembayaranLebih', 'ReservasiController@indexpembayaranlebih');
    Route::post('/UbahPembayaran', 'ReservasiController@ubahpembayaran');
    //PRAMUWISATA
    Route::post('/RiwayatPramu', 'ReservasiController@tambahriwayat');
    //INFORMASI
    Route::get('/Informasi', 'InformasiController@index');
    Route::post('/UbahInformasi', 'InformasiController@update');
    //email
    Route::post('/email', 'AkunController@email');
    //upload sertifikat
    Route::post('/uploadsertifikat', 'AkunController@uploadsertifikat');
    //jadwal
    Route::post('/tambahjadwal', 'PaketWisataController@tambah');
    // BUAT MOBILE
    Route::group(['prefix' => 'mobile'], function () {
        Route::middleware(['check.clientkey'])->group(function () {
            // PAKET WISATA
            Route::get('tipe_wisata', 'Mobile\PaketWisataController@tipe_wisata');
            Route::get('paket_wisata', 'Mobile\PaketWisataController@paket_wisata');
            Route::get('paket_wisata/{kd_paketwisata}', 'Mobile\PaketWisataController@paket_wisata_by_id');
            Route::get('paket_wisata/tipe/{kd_tipewisata}', 'Mobile\PaketWisataController@paket_wisata_by_tipe');

            // LIST WISATA, KENDARAAN, PENGINAPAN BUAT FORM CUSTOM TRIP
            Route::get('obyek_wisata/{kd_birotravel}/{kd_destinasi}', 'Mobile\CustomTripController@obyek_wisata');
            Route::get('penginapan/{kd_birotravel}', 'Mobile\CustomTripController@penginapan');
            Route::get('transportasi/{kd_birotravel}', 'Mobile\CustomTripController@transportasi');

            // HISTORY
            Route::post('wp_suggestion', 'Mobile\CustomTripController@weighted_product');

            // SEARCH
            Route::get('paket_wisata/search/{query}', 'Mobile\PaketWisataController@paket_wisata_search');
        });
        Route::middleware(['auth:api'])->group(function () {
            Route::get('wisatawan/{kd_wisatawan}', 'Mobile\AkunController@wisatawan');
            Route::get('logout', 'Mobile\AkunController@logout');
            Route::post('reservasi/opentrip', 'Mobile\ReservasiController@opentrip_insert');
            Route::post('reservasi/customtrip', 'Mobile\ReservasiController@customtrip_insert');
            Route::post('review', 'Mobile\ReviewController@review');
            Route::post('update_profile', 'Mobile\AkunController@update_profile');
            Route::post('payment', 'Mobile\PaymentController@upload');
            Route::get('history/wisatawan/{kd_wisatawan}', 'Mobile\ReservasiController@history_by_wisatawan');
            Route::get('history/{kd_reservasi}', 'Mobile\ReservasiController@history_by_id');
        });
        Route::post('login', 'Mobile\AkunController@login');
        Route::post('register', 'Mobile\AkunController@register');
    });
});